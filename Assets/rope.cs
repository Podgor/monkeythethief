﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rope : MonoBehaviour
{
    List<Vector3> pointsList;
    public GameObject tasks;
    LineRenderer line;
    public GameObject point;
    int count;

    bool draw;

    public void Drawposition()
    {

        line = GetComponent<LineRenderer>();
        pointsList = tasks.GetComponent<Boardtasks>().tasks;
        count = 1;
        line.positionCount = 1;
        line.SetPosition(0, pointsList[0]);
        DrawLine(count);

    }




    public void DrawLine(int i)
    {


        point.transform.position = new Vector3(pointsList[i - 1].x, pointsList[i - 1].y, pointsList[i - 1].z);


        draw = true;

        if (i <= pointsList.Count - 1)
        {
            line.positionCount = count + 1;
            line.SetPosition(count, point.transform.position);
            print("count " + count + " size  " + line.positionCount);
            iTween.MoveTo(point, iTween.Hash("x", pointsList[i].x, "y", pointsList[i].y, "z", 0, "time", 1f, "easetype", iTween.EaseType.easeInSine, "onComplete", "Nextpoint", "onCompleteTarget", gameObject));
        }




    }


    public void Nextpoint()
    {

        draw = false;

        if (count < pointsList.Count - 1)
        {
            count++;
            DrawLine(count);
        }

    }


    private void Update()
    {

        if (draw)
        {

            line.SetPosition(count, point.transform.position);
            // print(pointsList[count].x + "   " + pointsList[count].y + "   "  + pointsList[count].z );
        }
    }


}

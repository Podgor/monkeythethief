﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_shadowscrew : MonoBehaviour
{
    float delta_x;
    float delta_y;
    public bool rotatable = false;
    public bool active = false;
    public bool freeze_x = false;
    public bool freeze_y = false;
    public bool tapped = false;
    public bool rotate = false;
    public bool local = false;
    float delta;
    float delta_ang = 0;
    float delta_euler;
    float ang;
    public Vector3 target;
    public Vector3 target_rotation;
    public bool move = false;

    [System.NonSerialized]
    public bool check_double = false;
    public bool check_second_tap = false;


    public void OnMouseDrag()
    {
      
            Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            transform.position = new Vector3(mouse_pos.x - delta_x, mouse_pos.y - delta_y, transform.position.z);
            
    
        
    }
    private void OnMouseDown()
    {
       
        tapped = true;
       
   
            delta_x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            delta_y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
       


    }
    // Update is called once per frame
    private void OnMouseUp()
    {
        tapped = false;
     
    }

    

   

}

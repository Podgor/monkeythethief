﻿/******************************************************************************
 * Spine Runtimes License Agreement
 * Last updated January 1, 2020. Replaces all prior versions.
 *
 * Copyright (c) 2013-2020, Esoteric Software LLC
 *
 * Integration of the Spine Runtimes into software or otherwise creating
 * derivative works of the Spine Runtimes is permitted under the terms and
 * conditions of Section 2 of the Spine Editor License Agreement:
 * http://esotericsoftware.com/spine-editor-license
 *
 * Otherwise, it is permitted to integrate the Spine Runtimes into software
 * or otherwise create derivative works of the Spine Runtimes (collectively,
 * "Products"), provided that each user of the Products must obtain their own
 * Spine Editor license and redistribution of the Products in any form must
 * include this license and copyright notice.
 *
 * THE SPINE RUNTIMES ARE PROVIDED BY ESOTERIC SOFTWARE LLC "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL ESOTERIC SOFTWARE LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES,
 * BUSINESS INTERRUPTION, OR LOSS OF USE, DATA, OR PROFITS) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THE SPINE RUNTIMES, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

// Original Contribution by: Mitch Thompson

using Spine.Unity.AttachmentTools;
using Spine.Unity;
using System.Collections.Generic;
using UnityEngine;


public class Attach_sprite : MonoBehaviour
{
	public const string DefaultPMAShader = "Spine/Skeleton";
	public const string DefaultStraightAlphaShader = "Sprites/Default";

	#region Inspector
	public bool attachOnStart = true;
	public bool overrideAnimation = true;
	public Sprite sprite;
	[SpineSlot] public string slot;  //убрал т.к. не ищется нужный слот в инспекторе почему-то
	#endregion


	Spine.RegionAttachment attachment;
	Spine.Slot spineSlot;
	bool applyPMA;

	static Dictionary<Texture, Spine.AtlasPage> atlasPageCache;
	static Spine.AtlasPage GetPageFor(Texture texture, Shader shader)
	{
		if (atlasPageCache == null) atlasPageCache = new Dictionary<Texture, Spine.AtlasPage>();
		Spine.AtlasPage atlasPage;
		atlasPageCache.TryGetValue(texture, out atlasPage);
		if (atlasPage == null)
		{
			var newMaterial = new Material(shader);
			atlasPage = newMaterial.ToSpineAtlasPage();
			atlasPageCache[texture] = atlasPage;
		}
		return atlasPage;
	}

	void Start()
	{
		// Initialize slot and attachment references.
		Initialize(false);

		if (attachOnStart)
			Attach();
	}

	void AnimationOverrideSpriteAttach(ISkeletonAnimation animated)
	{
		if (overrideAnimation && isActiveAndEnabled)
			Attach();
	}

	public void Initialize(bool overwrite = true)
	{
		if (overwrite || attachment == null)
		{
			// Get the applyPMA value.
			var skeletonComponent = GetComponent<ISkeletonComponent>();
			var skeletonRenderer = skeletonComponent as SkeletonRenderer;
			if (skeletonRenderer != null)
				this.applyPMA = skeletonRenderer.pmaVertexColors;
			else
			{
				var skeletonGraphic = skeletonComponent as SkeletonGraphic;
				if (skeletonGraphic != null)
					this.applyPMA = skeletonGraphic.MeshGenerator.settings.pmaVertexColors;
			}

			// Subscribe to UpdateComplete to override animation keys.
			if (overrideAnimation)
			{
				var animatedSkeleton = skeletonComponent as ISkeletonAnimation;
				if (animatedSkeleton != null)
				{
					animatedSkeleton.UpdateComplete -= AnimationOverrideSpriteAttach;
					animatedSkeleton.UpdateComplete += AnimationOverrideSpriteAttach;
				}
			}

			spineSlot = spineSlot ?? skeletonComponent.Skeleton.FindSlot(slot); 
			Shader attachmentShader = applyPMA ? Shader.Find(DefaultPMAShader) : Shader.Find(DefaultStraightAlphaShader);
			if (sprite == null)
				attachment = null;
			else
				attachment = applyPMA ? sprite.ToRegionAttachmentPMAClone(attachmentShader) : sprite.ToRegionAttachment(Attach_sprite.GetPageFor(sprite.texture, attachmentShader));
		}
	}

	void OnDestroy()
	{
		var animatedSkeleton = GetComponent<ISkeletonAnimation>();
		if (animatedSkeleton != null)
			animatedSkeleton.UpdateComplete -= AnimationOverrideSpriteAttach;
	}

	/// <summary>Update the slot's attachment to the Attachment generated from the sprite.</summary>
	public void Attach()
	{
		if (spineSlot != null)
			spineSlot.Attachment = attachment;
	}

}


public static class SpriteAttachmentExtensions
{
	[System.Obsolete]
	public static Spine.RegionAttachment AttachUnitySprite(this Spine.Skeleton skeleton, string slotName, Sprite sprite, string shaderName = Attach_sprite.DefaultPMAShader, bool applyPMA = true, float rotation = 0f)
	{
		return skeleton.AttachUnitySprite(slotName, sprite, Shader.Find(shaderName), applyPMA, rotation: rotation);
	}

	[System.Obsolete]
	public static Spine.RegionAttachment AddUnitySprite(this Spine.SkeletonData skeletonData, string slotName, Sprite sprite, string skinName = "", string shaderName = Attach_sprite.DefaultPMAShader, bool applyPMA = true, float rotation = 0f)
	{
		return skeletonData.AddUnitySprite(slotName, sprite, skinName, Shader.Find(shaderName), applyPMA, rotation: rotation);
	}

	[System.Obsolete]
	public static Spine.RegionAttachment AttachUnitySprite(this Spine.Skeleton skeleton, string slotName, Sprite sprite, Shader shader, bool applyPMA, float rotation = 0f)
	{
		Spine.RegionAttachment att = applyPMA ? sprite.ToRegionAttachmentPMAClone(shader, rotation: rotation) : sprite.ToRegionAttachment(new Material(shader), rotation: rotation);
		skeleton.FindSlot(slotName).Attachment = att;
		return att;
	}

	[System.Obsolete]
	public static Spine.RegionAttachment AddUnitySprite(this Spine.SkeletonData skeletonData, string slotName, Sprite sprite, string skinName, Shader shader, bool applyPMA, float rotation = 0f)
	{
		Spine.RegionAttachment att = applyPMA ? sprite.ToRegionAttachmentPMAClone(shader, rotation: rotation) : sprite.ToRegionAttachment(new Material(shader), rotation);

		var slotIndex = skeletonData.FindSlot(slotName).Index;
		Spine.Skin skin = skeletonData.DefaultSkin;
		if (skinName != "")
			skin = skeletonData.FindSkin(skinName);

		if (skin != null)
			skin.SetAttachment(slotIndex, att.Name, att);

		return att;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class active_obj : MonoBehaviour
{
    // Start is called before the first frame update
    float delta_x;
    float delta_y;
    public bool rotatable = false;
    public bool active = false;
    public bool freeze_x = false;
    public bool freeze_y = false;
    public bool tapped = false;
    public bool rotate = false;
    public bool local = false;
    float delta;
    float delta_ang = 0;
    float delta_euler;
    float ang;
    public bool xAxis, yAxis, zAxis;
    public Vector3 target;
    public Vector3 target_rotation;
    public bool move = false;

    [System.NonSerialized]
    public bool check_double = false;
    public bool check_second_tap = false;

    void Start()
    {

    }
    public void OnMouseDrag()
    {
        if (active)
        {
            Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (freeze_x)
            {
                transform.position = new Vector3(transform.position.x, mouse_pos.y - delta_y, transform.position.z);
            }
            else if (freeze_y)
            {
                transform.position = new Vector3(mouse_pos.x - delta_x, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(mouse_pos.x - delta_x, mouse_pos.y - delta_y, transform.position.z);
            }
        }

        else if (!active && rotatable)
        {
            Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (transform.position.x > mouse_pos.x)
            {
                delta = (transform.position.y - mouse_pos.y) / (transform.position.x - mouse_pos.x);
                ang = Mathf.Atan(delta) * 180 / 3.1415f + 90;

            }
            else
            {
                delta = (transform.position.y - mouse_pos.y) / (transform.position.x - mouse_pos.x);
                ang = Mathf.Atan(delta) * 180 / 3.1415f + 270;

            }
            if (delta_euler >= 0)
            {
                
                transform.localRotation = Quaternion.Euler(Convert.ToInt32(xAxis) * (ang + delta_euler - delta_ang), Convert.ToInt32(yAxis) * (ang + delta_euler - delta_ang), Convert.ToInt32(zAxis) * (ang + delta_euler - delta_ang));
                Debug.Log(delta_ang.ToString() + "  " + ang.ToString());
            }
            else
            {
                transform.localRotation = Quaternion.Euler(Convert.ToInt32(xAxis) * (ang + (360 + delta_euler) - delta_ang), Convert.ToInt32(yAxis) * (ang + (360 + delta_euler) - delta_ang), Convert.ToInt32(zAxis) * (ang + (360 + delta_euler) - delta_ang));
                Debug.Log(delta_ang.ToString() + "  " + ang.ToString());
            }
        }
    }
    private void OnMouseDown()
    {
        Debug.Log( this.gameObject.name + " pressed");
        tapped = true;
        if (check_double)
        {
            check_second_tap = true;
            check_double = false;
        }
        else
        {
            check_double = true;
            Invoke("second_tap_delay", 0.3f);

        }

        if (active)
        {
            delta_x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            delta_y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
        }
        else if (!active && rotatable)
        {
            Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (transform.position.x > mouse_pos.x)
            {
                delta = (transform.position.y - mouse_pos.y) / (transform.position.x - mouse_pos.x);
                delta_ang = Mathf.Atan(delta) * 180 / 3.1415f + 90;
            }
            else
            {
                delta = (transform.position.y - mouse_pos.y) / (transform.position.x - mouse_pos.x);
                delta_ang = Mathf.Atan(delta) * 180 / 3.1415f + 270;
            }
            delta_euler = transform.localEulerAngles.z;
        }


    }
    // Update is called once per frame
    private void OnMouseUp()
    {
        tapped = false;
        check_second_tap = false;
    }

    void second_tap_delay()
    {
        check_double = false;
    }

    public void switch_()
    {
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }

    public void stop_rot()
    {
        rotate = false;
    }

    public void start_rot()
    {
        rotate = true;
    }

    public void activate()
    {
        active = true;
    }

    public void disactivate()
    {
        active = false;
    }

    public void django()
    {
        transform.SetParent(null);
    }

    public void parent(Transform t)
    {
        transform.SetParent(t);
    }


    void Update()
    {
        if (move)
        {
            if (local)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, target, 0.8f);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, target, 0.8f);
            }
            
        }
        if (move)
        {
            if (!local && System.Math.Abs(transform.position.x - target.x) <= 0.1f && System.Math.Abs(transform.position.y - target.y) <= 0.1f)
            {
                move = false;
            }
            if (local && System.Math.Abs(transform.localPosition.x - target.x) <= 0.1f && System.Math.Abs(transform.localPosition.y - target.y) <= 0.1f)
            {
                move = false;
            }

        }
        if (rotate)
        {
            transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, target_rotation, 0.2f);
        }
        if (rotate && System.Math.Abs(transform.eulerAngles.z - target_rotation.z) <= 1f)
        {
            rotate = false;
        }


    }

    

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swipe_condition : condition
{
    public Vector2 point;
    public Vector2 target;
    Vector2 start_pos;
    Vector2 end_pos;
    bool ended;
    public float approx = 1f;
    public float direction_approx = 30;

    public override bool check()
    {
        
        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    start_pos = Camera.main.ScreenToWorldPoint(touch.position);
                    ended = false;

                    break;

                case TouchPhase.Moved:

                    break;

                case TouchPhase.Ended:
                    ended = true;
                    end_pos = Camera.main.ScreenToWorldPoint(touch.position);
                    break;

                default:

                    Debug.Log("touch breaked");
                    break;
            }


        }
        if (ended && System.Math.Abs(start_pos.x - point.x) <= approx && System.Math.Abs(start_pos.y - point.y) <= approx && System.Math.Abs(System.Math.Atan(System.Math.Abs(end_pos.y-start_pos.y)/System.Math.Abs(end_pos.x - start_pos.x)) - System.Math.Atan(System.Math.Abs(target.y - point.y) / System.Math.Abs(point.x))) <= direction_approx && (System.Math.Abs(end_pos.x - start_pos.x) >= 1|| System.Math.Abs(end_pos.y - start_pos.y) >= 1))
        {
            ended = false;
            Debug.Log("swipe-swipe, motherfucker");
            return true;
            
        }
        else
        {
            return false;
        }

    }

    
}

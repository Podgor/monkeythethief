﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transform_aim : aim
{
    //[System.NonSerialized]
    public Vector2 pos;
    public float rot;
    public GameObject G;

    public void Start()
    {
        pos = transform.position;
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(transform.position, 0.2f);
        
    }

    public transform_aim(GameObject G, Vector2 pos, float rot)
    {
        this.pos = pos;
        this.rot = rot;
    }

    public override void apply()
    {
        //Debug.Log("trapple");
        G.GetComponent<active_obj>().move = true;
        G.GetComponent<active_obj>().target = pos;
        G.transform.rotation = Quaternion.Euler(0, 0, rot);
    }
}

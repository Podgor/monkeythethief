﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tap_up_condition : condition
{
    public GameObject obj;
    public bool firstly_tapped = false;

    public tap_up_condition(GameObject obj)
    {
        this.obj = obj;
    }

    public override bool check()
    {
        if (obj.GetComponent<active_obj>().tapped && !firstly_tapped)
        {
            firstly_tapped = true;
            Debug.Log("firstly");
            return false;
        }
        else if (!obj.GetComponent<active_obj>().tapped && firstly_tapped)
        {
            Debug.Log("tap_upped");
            firstly_tapped = false;
            return true;
        }
        else
        {
            return false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transform_condition : condition
{
    public GameObject obj;
    //[System.NonSerialized]
    public Vector2 pos;
    public float rot;
    public bool use_local = false;
    public float approx = 0.4f;
    public bool doesnt_need_to_untap = false;

    public transform_condition(GameObject obj, Vector2 pos, float rot)
    {
        this.obj = obj;
        this.pos = pos;
        this.rot = rot;

    }

    public override void Start()
    {
        pos = (!use_local) ? transform.position : transform.localPosition;

        base.Start();
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position, new Vector3(approx*2, approx*2));
        Gizmos.DrawSphere(transform.position, 0.12f);
    }

    public override bool check()
    {
        if (doesnt_need_to_untap)
        {
            if (!use_local)
            {
                if (System.Math.Abs(obj.transform.position.x - pos.x) <= approx && System.Math.Abs(obj.transform.position.y - pos.y) <= approx && System.Math.Abs(obj.transform.eulerAngles.z - rot) <= 30)
                {
                    //Debug.Log("true");


                    return true;

                }
                else
                {
                    return false;
                }
            }

            else
            {
                //Debug.Log(obj.transform.localPosition.y -pos.y);
                if (System.Math.Abs(obj.transform.localPosition.x - pos.x) <= approx && System.Math.Abs(obj.transform.localPosition.y - pos.y) <= approx && System.Math.Abs(obj.transform.localEulerAngles.z - rot) <= 30)
                {
                    //Debug.Log("true");


                    return true;
                    
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            if (!use_local)
            {
                if (System.Math.Abs(obj.transform.position.x - pos.x) <= approx && System.Math.Abs(obj.transform.position.y - pos.y) <= approx && System.Math.Abs(obj.transform.rotation.eulerAngles.z - rot) <= 30 && obj.GetComponent<active_obj>().tapped == false)
                {
                    //Debug.Log("true");
                    obj.transform.position = pos;
                    obj.transform.rotation = Quaternion.Euler(0, 0, rot);
                    return true;

                }
                else
                {
                    return false;
                }
            }

            else
            {
                //Debug.Log(obj.transform.localPosition.y -pos.y);
                if (System.Math.Abs(obj.transform.localPosition.x - pos.x) <= approx && System.Math.Abs(obj.transform.localPosition.y - pos.y) <= approx && System.Math.Abs(obj.transform.localEulerAngles.z - rot) <= 30 && obj.GetComponent<active_obj>().tapped == false)
                {
                    //Debug.Log("true");
                    obj.transform.localPosition = pos;
                    obj.transform.rotation = Quaternion.Euler(0, 0, rot);
                    return true;

                }
                else
                {
                    return false;
                }
            }
        }

        

    }
}
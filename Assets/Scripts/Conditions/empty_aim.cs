﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class empty_aim : aim
{
    public UnityEvent action;

    public override void apply()
    {
        action.Invoke();
    }
}

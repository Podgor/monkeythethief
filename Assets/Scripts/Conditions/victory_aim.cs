﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class victory_aim : aim
{
    public override void apply()
    {

        GameObject vict_pref = Resources.Load<GameObject>("victory_prefab");
        GameObject vict_obj = Instantiate(vict_pref, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0), Quaternion.identity);
        Invoke("complete", 1.2f);



    }
    void complete()
    {
        Global.complete_level();
    }
}

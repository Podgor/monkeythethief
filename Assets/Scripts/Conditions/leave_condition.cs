﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leave_condition : condition
{
    public GameObject obj;
    //[System.ser
    public Vector2 pos;
    public float rot;
    public bool use_local = false;
    public float approx = 0.3f;
    public bool doesnt_need_to_untap = false;

    public override void Start()
    {
        pos = (!use_local) ? transform.position : transform.localPosition;

        base.Start();
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position, new Vector3(approx * 2, approx * 2));
        Gizmos.DrawSphere(transform.position, 0.12f);
    }

    public override bool check()
    {
        if (!doesnt_need_to_untap)
        {
            if (!use_local)
            {
                if ((System.Math.Abs(obj.transform.position.x - pos.x) >= approx || System.Math.Abs(obj.transform.position.y - pos.y) >= approx) && obj.GetComponent<active_obj>().tapped == false)
                {

                    return true;

                }
                else
                {
                    return false;
                }
            }

            else
            {

                if ((System.Math.Abs(obj.transform.localPosition.x - pos.x) >= approx || System.Math.Abs(obj.transform.localPosition.y - pos.y) >= approx) && obj.GetComponent<active_obj>().tapped == false)
                {

                    return true;

                }
                else
                {
                    return false;
                }
            }
        }

        else
        {
            if (!use_local)
            {
                if ((System.Math.Abs(obj.transform.position.x - pos.x) >= approx || System.Math.Abs(obj.transform.position.y - pos.y) >= approx))
                {
                    //Debug.Log("chf,jnfkj");
                    return true;

                }
                else
                {
                    return false;
                }
            }

            else
            {

                if ((System.Math.Abs(obj.transform.localPosition.x - pos.x) >= approx || System.Math.Abs(obj.transform.localPosition.y - pos.y) >= approx))
                {
                    
                    return true;
                    

                }
                else
                {
                    return false;
                }
            }
        }
        

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class loose_aim : aim
{

    GameObject loose_pref;
    GameObject loose_obj;
    public bool reload = false;
    public override void apply()
    {
        
        //GameObject.Find("Canvas/Loose").GetComponent<scaling>().End();
        loose_pref = Resources.Load<GameObject>("loose_prefab");
        loose_obj = Instantiate(loose_pref, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0), Quaternion.identity);
        Invoke("complete", 1f);

    }
    void complete()
    {
        Destroy(loose_obj);
        if (reload)
        {
            Global.restart();
        }
    }
}
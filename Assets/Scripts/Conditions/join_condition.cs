﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class join_condition : condition
{
    public GameObject G1, G2;
    public float approx = 0.4f;

    public join_condition(GameObject G1, GameObject G2)
    {
        this.G1 = G1;
        this.G2 = G2;
    }


    public override bool check()
    {
        if (System.Math.Abs(G1.transform.position.x - G2.transform.position.x) <= approx && System.Math.Abs(G1.transform.position.y - G2.transform.position.y) <= approx && G1.activeSelf && G2.activeSelf)
        {
            //G2.transform.position = G1.transform.position;
            return true;
        }
        else
        {
            return false;
        }
    }
}

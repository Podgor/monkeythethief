﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound_aim : aim
{
    public AudioClip sound;
    [System.NonSerialized]
    public bool sound_made = false;

    public float volume = 1;

    public void Start()
    {
        gameObject.AddComponent<AudioSource>();
        GetComponent<AudioSource>().clip = sound;
        GetComponent<AudioSource>().volume = volume;
    }

    public override void apply()
    {
        if (!sound_made)
        {
            GetComponent<AudioSource>().Play();
            sound_made = true;
        }
        
    }
}

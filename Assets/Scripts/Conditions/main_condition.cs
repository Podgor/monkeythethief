﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEditor;


public class main_condition : MonoBehaviour
{
    public List<condition> cond_list = new List<condition>();
    public void Start()
    {
        Global.import_hints();

        cond_list.AddRange(FindObjectsOfType<condition>());
        if (Global.cur_level > 0)
        {
            Canvas can = Resources.Load<Canvas>("Canvas_prefab");
            Canvas the_can = Instantiate(can, Vector3.zero, Quaternion.identity);

        }
        Canvas back = Resources.Load<Canvas>("Background_prefab");
        Canvas the_back = Instantiate(back, Vector3.zero, Quaternion.identity);
        the_back.name = "maincam_background";
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        the_back.GetComponent<Canvas>().worldCamera = cam;
        
        //gameObject.AddComponent<AudioSource>();
        //GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("kwartet");
        //GetComponent<AudioSource>().loop = true;
        //GetComponent<AudioSource>().volume = 0.3f;
        //GetComponent<AudioSource>().Play();
        //foreach (SpriteRenderer sprite in GameObject.FindObjectsOfType<SpriteRenderer>())
        //{
        //    sprite.gameObject.transform.position = new Vector3(sprite.gameObject.transform.position.x, sprite.gameObject.transform.position.y, sprite.sortingOrder * -1);
        //}
    }

    

    public void Update()
    {
        foreach (condition the_dog in cond_list)
        {
            //if (the_dog.aims.Count <= 0)
            //{
            //    the_dog.aims.AddRange(the_dog.GetComponentsInChildren<aim>());
            //}

            if (the_dog.acted == false && the_dog.check_able())
            {
                if (the_dog.check())
                {
                    the_dog.act();
                    if (the_dog.once)
                    {
                        the_dog.acted = true;
                    }
                }

                else
                {
                    foreach (sound_aim sound_ in the_dog.GetComponentsInChildren<sound_aim>())
                    {
                        sound_.sound_made = false;
                    }
                }



                //if (the_dog.check())
                //{
                //    if (the_dog.chain == null && the_dog.block_chain == null)
                //    {
                //        the_dog.act();
                //        if (the_dog.once)
                //        {
                //            the_dog.acted = true;
                //        }
                //    }
                //    else if (the_dog.chain != null && the_dog.block_chain == null)
                //    {
                //        if (the_dog.chain.acted)
                //        {
                //            the_dog.act();
                //            if (the_dog.once)
                //            {
                //                the_dog.acted = true;
                //            }
                //        }
                //    }
                //    else if (the_dog.chain == null && the_dog.block_chain != null)
                //    {
                //        if (the_dog.block_chain.acted == false)
                //        {
                //            the_dog.act();
                //            if (the_dog.once)
                //            {
                //                the_dog.acted = true;
                //            }
                //        }
                //    }
                //    else
                //    {
                //        if (the_dog.chain.acted == true && the_dog.block_chain.acted == false)
                //        {
                //            the_dog.act();
                //            if (the_dog.once)
                //            {
                //                the_dog.acted = true;
                //            }
                //        }
                //    }

                //}
            }

            
        }
    }
}


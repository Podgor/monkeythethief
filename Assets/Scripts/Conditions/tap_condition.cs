﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tap_condition : condition
{
    public GameObject obj;
    public bool если_обжект_исчезает = false;

    public tap_condition(GameObject obj)
    {
        this.obj = obj;
    }

    public override bool check()
    {
        if (obj.GetComponent<active_obj>().tapped)
        {
            //Debug.Log("tap me man? yeah!");
            return true;
        }
        else
        {
            return false;
        }
    }

    public override void act()
    {
        if (если_обжект_исчезает)
        {
            obj.GetComponent<active_obj>().tapped = false;
        }
        base.act();
    }
}

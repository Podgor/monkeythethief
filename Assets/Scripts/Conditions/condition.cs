﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class condition : MonoBehaviour
{
    public List<aim> aims = new List<aim>();
    public bool once = true;
    public bool acted = false;
    public List<condition> chain = new List<condition>();
    public List<condition> block_chain = new List<condition>();

    public virtual void Start()
    {
        aims.AddRange(GetComponentsInChildren<aim>());
    }

    public virtual void act()
    {
        foreach (aim a in aims)
        {
            a.delaying();

        }
    }

    public abstract bool check();

    public bool check_able()
    {
        if ((chain.Count == 0 || chain.TrueForAll(c => c == null || c.acted)) && (block_chain.Count==0 || block_chain.TrueForAll(c => c == null || c.acted == false)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}

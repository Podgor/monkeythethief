﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class double_tap_cond : condition
{
    public GameObject obj;

    public override bool check()
    {
        if (obj.GetComponent<active_obj>().check_second_tap)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

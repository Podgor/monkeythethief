﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class aim : MonoBehaviour
{

    public float delay = 0;
    public abstract void apply();
    public void delaying()
    {
        Invoke("apply", delay);
    } 

}
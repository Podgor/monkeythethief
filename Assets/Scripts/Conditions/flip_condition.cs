﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class flip_condition : condition
{
    public Vector3 settings;

    public override bool check()
    {
        Debug.Log(settings);
        Vector3 acceleration = Input.acceleration;
        if (Math.Abs(acceleration.x - settings.x) <=0.2 && Math.Abs(acceleration.y - settings.y) <= 0.2 && Math.Abs(acceleration.z - settings.z) <= 0.2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

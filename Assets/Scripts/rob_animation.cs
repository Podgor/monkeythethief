﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using System.Linq;

public class rob_animation : MonoBehaviour
{

    //[SpineAnimation] public string[] relaxAnimation_F, relaxAnimation_B;

    //[SpineAttachment(true, slotField: "mask")] public string mask_on_B, mask_on_F;
    //[SpineAttachment(true, slotField: "mask")] public string mask_off_B, mask_off_F;

    [SpineSlot] public string mask, blink, mouth;
    [SpineAttachment(currentSkinOnly: true, slotField: "blink")] public string close0, close1, close2;
    [SpineAttachment(currentSkinOnly: true, slotField: "mouth")] public string mouth0, mouth5;


    [SpineSkin] public string mask_off, mask_on;

    SkeletonAnimation Rob_anim;


    TrackEntry entry;
    Spine.AnimationState animationState;

    //Vector3 pos;


    GameObject TimeManager;


    public bool sorting_order_fix, simple_pick;



    Dictionary<string, List<string>> animations_ = new Dictionary<string, List<string>>();
    
    public float timer;

    public string[] states, masked;
    public int state;
    public int mask_state;
    bool touch;
    public GameObject sprite_to_take, jump_target;
    Vector3 sprite_to_take_pos;
    public bool attached;

 

    void Start()
    {
        Rob_anim = GetComponent<SkeletonAnimation>();
        states = new string[2] { "_F", "_B" };
      
        masked = new string[2] { "_off", "_on" };

        SkeletonDataAsset asset = Rob_anim.SkeletonDataAsset;
        SkeletonData data = asset.GetSkeletonData(false);


        foreach (Spine.Animation animation in data.Animations)
        {

            if (animation.Name.Substring(0, 1) == "#")
            {
                string key = animation.Name.Substring(1, 7);

                if (!animations_.ContainsKey(key))

                {

                    animations_.Add(key, new List<string>());
                    animations_[key].Add(animation.Name);
                    //print("создан ключ " + animations_[key][0]);
                    print("создан ключ " + key);
                }

                else
                {
                    animations_[key].Add(animation.Name);
                    print("добавлено в ключ " + key);
                    //print("добавлено в ключ " + animations_[key][animations_[key].Count - 1]);
                }
                
                    
            }
        }

        entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);

        if (state == 0)
        {
            StartCoroutine("Blink", 0.2f);

            StartCoroutine("Lick", 0.5f);
        }

        StartCoroutine("Relax", 0.1f);



        Rob_anim.Skeleton.FindSlot("mask");
        if (mask_state == 1)
        {
            Rob_anim.Skeleton.SetSkin(mask_on);
            Rob_anim.Skeleton.SetAttachment("mask", "png/2x/mask" + masked[mask_state] + "/mask" + states[state]);
            print("png/2x/mask" + masked[mask_state] + "/mask" + states[state]);
        }
        //Rob_anim.Skeleton.FindSlot("mask");
        // Rob.SetAttachment(mask, aa);
        //Rob_anim.AnimationName = breathAnimation;
        // float timeScale = Rob_anim.timeScale;
        //TrackEntry entry = Rob.GetComponent<SkeletonAnimation>().
        //TrackEntry entry = Rob_anim.AnimationState.SetAnimation(1, "breath",true);

        // entry = Rob_anim.AnimationState.AddAnimation(1, "laugh", false, 3);





    }



    public void Relax_()
    {
        StartCoroutine("Relax", 0.1f);
    }

    public IEnumerator Relax()
    {



        while (true)
        {

            yield return new WaitForSeconds(Random.Range(10f, 60f));

            if (!Global.busy)
            {
                Debug.Log("relax_start");
                string key = "relax" + states[state];
                entry = Rob_anim.AnimationState.SetAnimation(0, animations_[key][(int)Random.Range(0, animations_[key].Count)], false);

                //Debug.Log("relax " + Rob_anim.name);
                yield return new WaitForSpineAnimationComplete(entry);
                entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);
                Rob_anim.Skeleton.SetAttachment(blink, close0);

            }
            else
                Debug.Log("relax_canceled");

        }

    }


    public void Turn()
    {
        StopCoroutine("Blink");
      
        StopCoroutine("Lick");
        StartCoroutine("Turn_", 0.1f);
    }

    public IEnumerator Turn_()
    {

        entry = Rob_anim.AnimationState.SetAnimation(0, "turn" + states[state], false);
        yield return new WaitForSpineAnimationComplete(entry);
        if (state == 0) state = 1;

        else
        {
            state = 0;
            Rob_anim.Skeleton.SetAttachment(blink, close0);
            StartCoroutine("Blink", 0.2f);

            StartCoroutine("Lick", 0.5f);
        }
        //Debug.Log ("turned to " + states[state]);

        entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);
        Rob_anim.Skeleton.SetAttachment(blink, close0);
    }


    public void Mask()
    {
        if(mask_state ==0)
        {
            Rob_anim.Skeleton.SetSkin(mask_on);
            mask_state = 1;
            
        }
        else
        {
            Rob_anim.Skeleton.SetSkin(mask_off);
            mask_state = 0;
            
        }
        Rob_anim.Skeleton.SetAttachment("mask", "png/2x/mask" + masked[mask_state] + "/mask" + states[state]);
    }

    void OnMouseUp()
    {

        touch = false;
        //transform.position = pos;
        entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);

        StopCoroutine("Blink");
       
        StopCoroutine("Lick");
        StopCoroutine("Drag");
        if (state ==0)
        {
            StartCoroutine("Blink", 0.2f);
            
            StartCoroutine("Lick", 0.5f);
        }
        StartCoroutine("Relax", 0.1f);
        Rob_anim.Skeleton.SetAttachment(blink, close0);
    }

    void OnMouseDown()
    {
        StopCoroutine("Lick");
        StopCoroutine("Relax");
        Rob_anim.Skeleton.SetAttachment(blink, close0);
        //delta = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y, 0);
        entry = Rob_anim.AnimationState.SetAnimation(0, "touch" + states[state], false);
        touch = true;
        //pos = transform.position;
        StartCoroutine("Drag");
   
    }
    public IEnumerator Drag()
    {
        Rob_anim.Skeleton.SetAttachment(blink, close0);
        //Debug.Log("touch");
        yield return new WaitForSpineAnimationComplete(entry);
        //Debug.Log("drag");
        if (touch) entry = Rob_anim.AnimationState.SetAnimation(0, "drag" + states[state], true);
        //yield return new WaitForSpineAnimationComplete(entry);
    }

    


    public void Pickup()
    {
        if (!attached)
        {
            sprite_to_take_pos = sprite_to_take.transform.position;
            StopCoroutine("Relax");
            StopCoroutine("Blink");
        
            StopCoroutine("Lick");
            Rob_anim.Skeleton.SetAttachment(blink, close0);
            if (simple_pick) StartCoroutine("_Pickup_simple");
            else StartCoroutine("_Pickup");



        }

        
    }

    public IEnumerator _Pickup()
    {
        int n;
        string side;
        int temp_state = 0;
        Vector2 direct;

        GetComponent<Attach_sprite>().sprite = sprite_to_take.GetComponent<SpriteRenderer>().sprite;

        if (sprite_to_take.transform.position.y > transform.position.y + 0.6f)
        {



            temp_state = 1;
            direct = new Vector2(sprite_to_take.transform.position.x - transform.position.x + 0.95f, sprite_to_take.transform.position.y - transform.position.y - 0.8f);
            if (!sorting_order_fix) sprite_to_take.GetComponent<SpriteRenderer>().sortingOrder = Rob_anim.GetComponent<MeshRenderer>().sortingOrder - 1;
            if (direct.x > 0) side = "_R";
            else side = "_L";


        }

        else
        {


            if (sprite_to_take.transform.position.x - transform.position.x < 1.5f)
            {
                direct = new Vector2(sprite_to_take.transform.position.x - transform.position.x + 0.1f, sprite_to_take.transform.position.y - transform.position.y - 0.5f);
                side = "_R";

                if (!sorting_order_fix) sprite_to_take.GetComponent<SpriteRenderer>().sortingOrder = Rob_anim.GetComponent<MeshRenderer>().sortingOrder + 1;
            }

            else
            {

                side = "_L";
                direct = new Vector2(sprite_to_take.transform.position.x - transform.position.x - 1.5f, sprite_to_take.transform.position.y - transform.position.y - 0.25f);

            }


        }


        n = (int)(Mathf.Sqrt(direct.x * direct.x + direct.y * direct.y) / 0.7f);
        iTween.MoveAdd(gameObject, iTween.Hash("x", direct.x, "y", direct.y, "time", 0.208f * n, "delay", 0.02f, "easetype", iTween.EaseType.linear));

        for (int i = 0; i < n; i++)
        {

            entry = Rob_anim.AnimationState.SetAnimation(0, "step" + states[temp_state] + side, false);

            yield return new WaitForSpineAnimationComplete(entry);
        }

        Debug.Log("step+take " + states[temp_state] + " шагов  " + n);

        entry = Rob_anim.AnimationState.SetAnimation(0, "take1" + states[temp_state] + side, false);
        yield return new WaitForSpineAnimationComplete(entry);
        GetComponent<Attach_sprite>().Initialize(true);
        sprite_to_take.GetComponent<SpriteRenderer>().enabled = false;
        sprite_to_take.GetComponent<BoxCollider2D>().enabled = false;
        attached = true;
        entry = Rob_anim.AnimationState.SetAnimation(0, "take2" + states[temp_state] + side, false);
        yield return new WaitForSpineAnimationComplete(entry);
        if (temp_state != state)
        {
            entry = Rob_anim.AnimationState.SetAnimation(0, "turn" + states[temp_state], false);
            yield return new WaitForSpineAnimationComplete(entry);

        }

        entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);

        if (state == 0)
        {
            StartCoroutine("Blink", 0.2f);

            StartCoroutine("Lick", 0.5f);
        }

        StartCoroutine("Relax", 0.1f);

    }



    public IEnumerator _Pickup_simple()
    {

        GetComponent<Attach_sprite>().sprite = sprite_to_take.GetComponent<SpriteRenderer>().sprite;

        if (!sorting_order_fix) sprite_to_take.GetComponent<SpriteRenderer>().sortingOrder = Rob_anim.GetComponent<MeshRenderer>().sortingOrder + 1;

        entry = Rob_anim.AnimationState.SetAnimation(0, "take1" + states[state] + "_R", false);
        yield return new WaitForSpineAnimationComplete(entry);
        GetComponent<Attach_sprite>().Initialize(true);
        sprite_to_take.GetComponent<SpriteRenderer>().enabled = false;
        sprite_to_take.GetComponent<BoxCollider2D>().enabled = false;
        attached = true;
        entry = Rob_anim.AnimationState.SetAnimation(0, "take2" + states[state] + "_R", false);
        yield return new WaitForSpineAnimationComplete(entry);
        entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);

        if (state == 0)
        {
            StartCoroutine("Blink", 0.2f);

            StartCoroutine("Lick", 0.5f);
        }

        StartCoroutine("Relax", 0.1f);

    }


    public void PutDown()
    {
        if(attached)
               {
                    GetComponent<Attach_sprite>().sprite = null;
                    GetComponent<Attach_sprite>().Initialize(true);
                    sprite_to_take.GetComponent<SpriteRenderer>().enabled = true;
                    sprite_to_take.GetComponent<BoxCollider2D>().enabled = true;
                    if (state ==0) sprite_to_take.transform.position = new Vector2 (transform.position.x + 0.95f, transform.position.y - 0.8f);
                    else sprite_to_take.transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y + 1.2f);
                    attached = false;
                    
                }


    }

    public void Happy()
    {

        StopCoroutine("Relax");
        StopCoroutine("Blink");
        
        StopCoroutine("Lick");
        StartCoroutine("_Happy");

    }

    public IEnumerator _Happy()
    {

        string key = "good#" + states[state];
        Debug.Log("happy " + animations_[key][0]);
        entry = Rob_anim.AnimationState.SetAnimation(0, animations_[key][0], false);

        yield return new WaitForSpineAnimationComplete(entry);

        if (state == 0)
        {

            StartCoroutine("Blink", 0.2f);

            StartCoroutine("Lick", 0.5f);
        }

        StartCoroutine("Relax", 0.1f);


    }



    public void Unhappy()
    {
        StopCoroutine("Relax");
        StopCoroutine("Blink");
        
        StopCoroutine("Lick");
        StartCoroutine("_Unhappy");


    }

    
    public IEnumerator _Unhappy()
    {
        string key = "bad##" + states[state];
        entry = Rob_anim.AnimationState.SetAnimation(0, animations_[key][(int)Random.Range(0, animations_[key].Count)], false);
        yield return new WaitForSpineAnimationComplete(entry);
        if (state == 0)
        {
            StartCoroutine("Blink", 0.2f);
            
            StartCoroutine("Lick", 0.5f);
        }

        StartCoroutine("Relax", 0.1f);

    }


    public IEnumerator Blink(float t)
    {
       
            while (true && state == 0)
        {
                
                yield return new WaitForSeconds(t);
                Rob_anim.Skeleton.SetAttachment(blink, close1);
               
                
                yield return new WaitForSeconds(0.1f);

            Rob_anim.Skeleton.SetAttachment(blink, close2);
               
                yield return new WaitForSeconds(0.2f);
                Rob_anim.Skeleton.SetAttachment(blink, close0);
                t = Random.Range(1.2f, 10f);
                
            }

      




    }



    public IEnumerator Lick()
    {
        
            while (true || state == 0)
            {
                
                yield return new WaitForSeconds(Random.Range(5f, 10f));
                
               
                for (int i = 0; i < Random.Range(2f, 8f); i++)
                {
                    yield return new WaitForSeconds(0.05f);
                    Rob_anim.Skeleton.SetAttachment(mouth, mouth0);
                    yield return new WaitForSeconds(0.07f);
                    Rob_anim.Skeleton.SetAttachment(mouth, mouth5);
                    
                }
            //Debug.Log("lick " + states[state]);



        }

    }

    
    


    
    //public IEnumerator Relax_temp()
    //{

    //    //Debug.Log("relax_start");

        

    //        yield return new WaitForSeconds(0.5f);

    //        string key = "relax" + states[state];
    //        entry = Rob_anim.AnimationState.SetAnimation(0, animations_[key][(int)Random.Range(0, animations_[key].Count)], false);

    //        Debug.Log("relax " + Rob_anim.name);
    //        yield return new WaitForSpineAnimationComplete(entry);
    //        entry = Rob_anim.AnimationState.SetAnimation(0, "breath" + states[state], true);


       

    //}
 
       


    public void Step()
    {
        StopCoroutine("Relax");
        StartCoroutine("Step_", 0.2f);
        
    }
    public IEnumerator Step_()
    {
        
        iTween.MoveAdd(gameObject, iTween.Hash("x", 5f, "y", -5f, "time", 0.08f, "delay", 0.01f, "easetype", iTween.EaseType.easeInSine));
        entry = Rob_anim.AnimationState.SetAnimation(0, "step" + states[state], false);
        yield return new WaitForSpineAnimationComplete(entry);
        StartCoroutine("_Pickup_simple", 0.1f);
    }

    public void Jump()
    {
        
            StopCoroutine("Relax");
            StopCoroutine("Blink");
            StopCoroutine("Lick");
            Rob_anim.Skeleton.SetAttachment(blink, close0);
            StartCoroutine("Jump_", 2f);
            
    }
    public IEnumerator Jump_()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("New Path 1"), "time", 0.5f, "delay", 0.3f, "easetype", iTween.EaseType.easeInOutSine, "OnComplete", "Jump2_"));
        entry = Rob_anim.AnimationState.SetAnimation(0, "jump" + states[state], false);
        yield return new WaitForSpineAnimationComplete(entry);
        
 
        if (state == 0)
        {
            StartCoroutine("Blink", 0.2f);

            StartCoroutine("Lick", 0.5f);
        }

        StartCoroutine("Relax", 0.1f);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shadowswitch : MonoBehaviour
{

    GameObject shadow;
    GameObject pin;
    public void Statue_on()
    {
        
       
        shadow = GameObject.Find("columnwstatue_shadow");
        pin = shadow.transform.Find("pin").gameObject;
        pin.name = "shadow_pin";

        shadow.AddComponent<BoxCollider2D>();
        shadow.AddComponent<TriggerEnter>();
        shadow.AddComponent<active_obj>();
        
        shadow.GetComponent<BoxCollider2D>().isTrigger = true;
        shadow.GetComponent<BoxCollider2D>().offset = new Vector2 (0,-8);
        shadow.GetComponent<BoxCollider2D>().size = new Vector2(0.1f, 0.1f);
        shadow.GetComponent<SpriteRenderer>().enabled = true;
                
        GameObject.Find("column_shadow").GetComponent<SpriteRenderer>().enabled = false;
        pin.transform.localPosition = new Vector3(0, -8, 0);
        
    }


}

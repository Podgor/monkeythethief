﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vaseheating : MonoBehaviour
    
{
    bool flamed = false;
    bool broken = false;
    bool snakeishere = true;
    public GameObject tail;
    GameObject flame;
    public GameObject key;
    Vector3 pos;
    
    void Start()
    {
        
        flame = GameObject.Find("flame");
        pos = flame.transform.position;
    }

   
    public void Heat()
    {
        if (!flamed && !broken && key.activeSelf == false)
        {
           StartCoroutine(Delay());
        }
        if(flamed && snakeishere)
        {
            Snakego();
        }
    }

    IEnumerator Delay()
    {
        for (int i = 0; i <= 255; i += 3)
        {
            GetComponent<SpriteRenderer>().color = new Color32(255, (byte)(255 - i), (byte)(255 - i), 255);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        flamed = true;

    }

    void Snakego()
    {
        snakeishere = false;
        int n = int.Parse(name.Substring(4, 1));
        tail.SetActive(true);
        if (n < 5)
            {
            iTween.RotateAdd(tail, iTween.Hash("z", -360, "time", 0.7f, "delay", 0.7f, "onCompleteTarget", gameObject, "onComplete", "FlameOff", "easetype",  iTween.EaseType.easeInOutSine));
            }
        else
            {
            tail.GetComponent<SpriteRenderer>().flipX = false;
            iTween.RotateAdd(tail, iTween.Hash("z", 360, "time", 0.7f, "delay", 0.7f, "onCompleteTarget", gameObject, "onComplete", "FlameOff", "easetype", iTween.EaseType.easeInOutSine));
            }
        
        
        
    }


   void FlameOff()
    {
        broken = true;
        key.SetActive(true);
        key.transform.position = new Vector3 (transform.position.x, transform.position.y + 2 * GetComponent<SpriteRenderer>().bounds.extents.y, -1);
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/vasebroken");
        flame.GetComponent<BoxCollider2D>().enabled = false;
        key.GetComponent<BoxCollider2D>().enabled = true;
        key.GetComponent<SpriteRenderer>().sortingOrder = 10;
        tail.SetActive(false);
    }

  
    

    public void Cool()
    {
        GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
        flamed = false;
    }
    public void SpriteChange()
    {
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/vasebroken");
      
    }

}

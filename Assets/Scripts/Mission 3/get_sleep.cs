﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class get_sleep : MonoBehaviour
{
    public GameObject meduza_eyecover;
    // Start is called before the first frame update
    public void Increase()
    {
        GetComponent<SpriteRenderer>().sortingOrder = 16;
        iTween.ScaleTo(gameObject, iTween.Hash("x", transform.localScale.x * 5f, "y", transform.localScale.y * 5f, "delay", 1f, "time", 11.0f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleTo(gameObject, iTween.Hash("x", transform.localScale.x , "y", transform.localScale.y , "delay", 12f, "time", 3.0f, "easetype", iTween.EaseType.easeInOutSine));

    }
    public void EyeClose()
    {
        iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", -meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 0.5f, "delay", 2, "easetype", iTween.EaseType.easeOutQuint));
        iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 1, "delay", 3, "easetype", iTween.EaseType.easeOutQuint));
        iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", -meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 0.5f, "delay", 5, "easetype", iTween.EaseType.easeOutQuint));
        iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 1, "delay", 6, "easetype", iTween.EaseType.easeOutQuint));
        iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", -meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 1, "delay", 7, "easetype", iTween.EaseType.easeOutQuint));
        //iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 0.5f, "delay", 10, "easetype", iTween.EaseType.easeOutQuint));
        //iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", -meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 1.0f, "delay", 12, "easetype", iTween.EaseType.easeOutQuint));
        //iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 1.5f, "delay", 10, "easetype", iTween.EaseType.easeOutQuint));
        //iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", -meduza_eyecover.GetComponent<SpriteRenderer>().bounds.extents.y, "time", 1.5f, "delay", 12, "easetype", iTween.EaseType.easeOutQuint));
    }

}

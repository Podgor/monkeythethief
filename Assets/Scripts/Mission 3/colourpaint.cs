﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colourpaint : MonoBehaviour
{
    public bool green;
    //public bool red, green1, green2, green3;
    public string colouredname;
    public Color32 Scolor;
    public Color32 Ecolor;
    string temp = null;
    public Dictionary<string, (int, Color32)> dict = new Dictionary<string, (int, Color32)>
    {
        { "first", (50, new Color32(0, 255, 0, 255)) },
        { "second", (50, new Color32(0, 255, 0, 255)) },
        { "third", (20, new Color32(0, 255, 0, 255)) },
        { "star", (20, new Color32(255, 0, 0, 255)) }
    };
   
    


    public void Colorize()
    {

        if (colouredname != "")
        {
            GameObject a = GameObject.Find(colouredname);
            a.GetComponent<SpriteRenderer>().color = Ecolor;
            green = false;

            

        }

        if (new List<string>(dict.Keys).TrueForAll(x => GameObject.Find(x).GetComponent<SpriteRenderer>().color == dict[x].Item2))
        {
            GameObject b = GameObject.Find("brush");
            b.transform.position = new Vector3(10, 10, 0);
            b.GetComponent<brush>().enabled = false;
              
        }
       

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        colouredname = collision.gameObject.name;
        if (!green)
        {
            green = true;
            GetComponent<LineRenderer>().startColor = Scolor;
            GetComponent<LineRenderer>().endColor = Ecolor;
            GetComponent<DrawTree>().count = dict[colouredname].Item1;

        }
        else
        {
            temp = collision.gameObject.name;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
        colouredname = "";
        green = false;

    }
    
    public void Turnblue()
    {
        Scolor = new Color32 (0, 0, 100, 255);
        Ecolor = new Color32 (0, 0, 255, 255);
        GetComponent<SpriteRenderer>().color = new Color32 (0, 0, 255, 255);
    }
    public void Turngreen()
    {
        Scolor = new Color32(0, 100, 0, 255);
        Ecolor = new Color32 (0, 255, 0, 255);
        GetComponent<SpriteRenderer>().color = new Color32 (0, 255, 0, 255);
    }
    public void Turnred()
    {
        Scolor = new Color32(100, 0, 0,255);
        Ecolor = new Color32 (255, 0, 0, 255);
        GetComponent<SpriteRenderer>().color = new Color32(255, 0, 0, 255);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ecscursion : MonoBehaviour
{

    GameObject pin;

    public void AllGone()
    {
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
         
            float r1 = Random.Range(4f, 5f);
            float r2 = Random.Range(0f, 1f);
            iTween.ScaleTo(child.gameObject, iTween.Hash("y", 0.2f, "x", 0.2f, "time", r1, "delay", r2, "easetype", iTween.EaseType.easeOutQuint));
            iTween.MoveTo(child.gameObject, iTween.Hash("y", 1.54f, "x", 1.66f, "time", r1, "delay", r2, "easetype", iTween.EaseType.easeOutQuint));
        }
       
    }
    public void Monkeygo()
    {

        iTween.ScaleTo(gameObject, iTween.Hash("y", 0.1f, "x", 0.1f, "time", 5f, "easetype", iTween.EaseType.easeOutQuint));
        iTween.MoveTo(gameObject, iTween.Hash("y", 1.54f, "x", 1.66f, "time", 5f, "easetype", iTween.EaseType.easeOutQuint));
    }


   
  

    public void Hide()
    {
       pin = GameObject.Find("shadow_pin");
       transform.position = pin.transform.position;

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bolthit : MonoBehaviour
{
    public GameObject monkey;

    // Start is called before the first frame update
    public void Hit1()
    {
        iTween.ShakePosition(gameObject, iTween.Hash("y", 0.3f, "time", 0.8f));
        iTween.ShakeScale(gameObject, iTween.Hash("y", 0.3f, "time", 0.8f));

        

    }
    public void Hit2()
    {
        float deltax = transform.position.x - monkey.transform.position.x;
        float deltay = transform.position.y - monkey.transform.position.y;
        float arc = Mathf.Atan(deltax / deltay) * Mathf.Rad2Deg;
        iTween.RotateAdd(gameObject, iTween.Hash("z", 90 + arc, "time", 0.01f, "delay", 1f, "easetype", iTween.EaseType.easeInExpo));

        iTween.ShakePosition(gameObject, iTween.Hash("y", 0.7f, "time", 0.8f));
        iTween.ShakeScale(gameObject, iTween.Hash("y", 0.7f, "time", 0.8f));
        //iTween.RotateTo(gameObject, iTween.Hash("z", 72f, "time", 0.01f, "delay", 1f, "easetype", iTween.EaseType.easeInExpo));
        iTween.ScaleTo(gameObject, iTween.Hash("x", 5f, "y", 5f, "time", 0.2f, "delay", 1f, "easetype", iTween.EaseType.easeInExpo));
        iTween.MoveTo(gameObject, iTween.Hash("x", monkey.transform.position.x, "y", monkey.transform.position.y + 2, "time", 0.2f, "delay", 1f, "easetype", iTween.EaseType.easeInExpo));
        //iTween.ShakeScale(monkey, iTween.Hash("x", 0.1f, "y", 0.1f, "time", 1.8f, "delay", 1.2f));
        //iTween.ShakeScale(monkey, iTween.Hash("x", 0.1f, "y", 0.1f, "time", 1.8f, "delay", 1.3f));

        var clone1 = Instantiate(monkey);
        iTween.ShakeScale(clone1, iTween.Hash("x", 0.15f, "y", 0.15f, "time", 1.8f, "delay", 1.3f, "LoopType", "loop"));
        var clone2 = Instantiate(monkey);
        iTween.ShakeScale(clone2, iTween.Hash("x", 0.15f, "y", 0.15f, "time", 0.5f, "delay", 1.4f, "LoopType", "pingPong"));

    }

}

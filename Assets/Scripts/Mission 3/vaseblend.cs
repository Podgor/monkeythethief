﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vaseblend : MonoBehaviour
{


 
    public void Blend()
    {
        Vector3 pos = transform.position;
        iTween.MoveTo(gameObject, iTween.Hash("x", 0, "y", -3.9f, "time", 0.3f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ShakePosition(gameObject, iTween.Hash("x", 0.3f, "y", 0.3f, "time", 0.3f, "delay", 0.3f));
        iTween.MoveTo(gameObject, iTween.Hash("x", pos.x, "y", pos.y, "time", 0.3f, "delay", 0.6f, "easetype", iTween.EaseType.easeInOutSine));
    }
}

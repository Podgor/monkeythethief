﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public GameObject area;
    public GameObject aim1, aim2;
    public Vector3 scale, pos;
    bool a;

    public void Monkeyjump1()
    {
        if (!a)
        {
            a = true;
            scale = transform.localScale;
            pos = transform.position;
            area.SetActive(false);
            aim1.SetActive(false);
            
            

            iTween.MoveTo(gameObject, iTween.Hash("y", pos.y + aim1.transform.localPosition.y * 0.8f, "time", 0.15f, "easetype", iTween.EaseType.easeOutSine));
            iTween.ScaleTo(gameObject, iTween.Hash("x", scale.x, "y", scale.y, "time", 0.01f));
            iTween.MoveTo(gameObject, iTween.Hash("y", pos.y, "time", 0.3f, "delay", 0.2f, "easetype", iTween.EaseType.easeInSine, "onComplete", "Area_on", "onCompleteTarget", gameObject));
        }
        
    } 
    public void Monkeyjump2()
    {
        if (!a)
        {
            a = true;
            scale = transform.localScale;
            pos = transform.position;
            area.SetActive(false);
            
            aim2.SetActive(false);
            iTween.MoveTo(gameObject, iTween.Hash("y", pos.y + aim2.transform.localPosition.y *0.8f, "time", 0.15f, "easetype", iTween.EaseType.easeOutSine));
            iTween.ScaleTo(gameObject, iTween.Hash("x", scale.x, "y", scale.y, "time", 0.01f));
            iTween.MoveTo(gameObject, iTween.Hash("y", pos.y, "time", 0.3f, "delay", 0.2f, "easetype", iTween.EaseType.easeInSine, "onComplete", "Area_on", "onCompleteTarget", gameObject));
        }

    }
    void Area_on()
    {
        area.SetActive(true);
        aim1.SetActive(true);
        aim2.SetActive(true);
        a = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotator : MonoBehaviour
{
     //Vector2 firstPressPos;
     //Vector2 secondPressPos;
    // Vector2 currentSwipe;
    //public float ypos;
    public GameObject monkey;
    public GameObject area;
    //int direction = 0;

    private void Start()
    {
        iTween.RotateAdd(gameObject, iTween.Hash("z", 180, "time", 0.8f, "easetype", iTween.EaseType.easeInOutSine));

    }
    
    public void SimpleRotate()
    {
        //monkey.transform.SetParent(transform);
        
        monkey.transform.localPosition = new Vector3(0, -10f, 0);
        //area.SetActive(false);
        //monkey.GetComponent<BoxCollider2D>().enabled = false;
        iTween.RotateAdd(gameObject, iTween.Hash("z", 1440.0f, "time", 15.0f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));
        for(int i = 0; i < 5; i++)
        {
            var clone = Instantiate(monkey);
            clone.transform.localPosition = new Vector3(monkey.transform.localPosition.x + Random.Range(0.5f, 3f), monkey.transform.localPosition.y + Random.Range(-3f, 3f), 0);
            iTween.RotateAdd(clone, iTween.Hash("z", 1440.0f, "time", 15.0f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));

        }

    }

    void Jumpout ()
    {
        monkey.transform.SetParent(null);
        
        iTween.MoveTo(monkey, iTween.Hash("x", 0, "y", -6, "z", 0, "time", 0.3f, "easetype", iTween.EaseType.easeOutQuint));
        monkey.transform.localEulerAngles = new Vector3(0, 0, 0);
        area.SetActive(true);
        monkey.GetComponent<BoxCollider2D>().enabled = true;
       
    }
}

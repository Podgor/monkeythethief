﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Santacoming : MonoBehaviour
{
    public GameObject night;
    public GameObject monkey;

    void Start()
    {

        iTween.FadeTo(night, iTween.Hash("alpha", 0, "a", 0, "time", 0));


    }


    public void Night()
    {
      
      iTween.FadeTo(night, iTween.Hash("alpha", 0.5f, "time", 2, "delay", 0.3f, "onComplete", "Santa", "onCompleteTarget", gameObject));

    }

    void Santa()
    {

        iTween.MoveTo(gameObject, iTween.Hash("x", 0, "time", 1.5f, "easetype", iTween.EaseType.easeOutSine, "onComplete", "Turn"));

    }
    void Turn()
    {

        monkey.GetComponent<active_obj>().freeze_y = false;
        GameObject s = GameObject.Find("sack");
        s.transform.SetParent(null);
        iTween.MoveTo(s, iTween.Hash("y", -4, "time", 0.3f, "delay", 0.2f, "easetype", iTween.EaseType.easeOutQuint));
        GetComponent<SpriteRenderer>().flipX = true;
        iTween.MoveTo(gameObject, iTween.Hash("x", 10, "time", 2, "delay", 0.5f, "easetype", iTween.EaseType.easeOutQuint));
        iTween.FadeTo(night, iTween.Hash("alpha", 0, "time", 2, "delay", 1));
    }

    public void Monkeyjump()
    {
        iTween.MoveAdd(monkey, iTween.Hash("y", 4, "time", 0.15f, "easetype", iTween.EaseType.easeOutSine));
        iTween.ScaleAdd(monkey, iTween.Hash("x", 0.2f, "y", 0.2f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.MoveAdd(monkey, iTween.Hash("y", -6, "time", 0.3f, "delay", 0.2f, "easetype", iTween.EaseType.easeInSine));
    }
   
    

}

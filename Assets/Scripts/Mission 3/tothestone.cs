﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tothestone : MonoBehaviour
{
    public GameObject meduza_eyecover;
    
    public GameObject monkeybody;
    public GameObject flush2;
    public GameObject flush1;
    // Start is called before the first frame update
    void Start()
    {
        iTween.MoveAdd(meduza_eyecover, iTween.Hash("y", + 0.2f, "time", 0.1f, "delay", 0f, "easetype", iTween.EaseType.easeOutQuint));
       
    }

    // Update is called once per frame

    public void GetStuck()
    {
        flush1.SetActive(true);
        flush2.SetActive(true);
        flush1.GetComponent<ParticleSystem>().Play();
        flush2.GetComponent<ParticleSystem>().Play();
        StartCoroutine(Delay());
        

    }

    IEnumerator Delay()
    {
        for (int i = 0; i <= 150; i += 1)
        {
            monkeybody.GetComponent<SpriteRenderer>().color = new Color32((byte)(255 - i), (byte)(255 - i), (byte)(255 - i), 255);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        GetComponent<BoxCollider2D>().enabled = false;
    }
    //void Stone()
    //{
    //    var clone = Instantiate(monkeybody);
    //    clone.GetComponent<SpriteRenderer>().color = new Color32(0, 255, 0, 100);
    //    clone.GetComponent<SpriteRenderer>().sortingOrder = monkeybody.GetComponent<SpriteRenderer>().sortingOrder + 1;
    //    StartCoroutine(Delay());
    //}

    
    public void TakeSkin()
    {
        monkeybody.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/sheep");
    }
}

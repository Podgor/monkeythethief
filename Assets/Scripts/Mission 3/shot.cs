﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shot : MonoBehaviour
{
    GameObject crack;
    GameObject spoon;
    // Start is called before the first frame update
    public void Throw ()
    {
        
        if (name.Substring(0, 4) == "ball")
        {
            crack = GameObject.Find("crack" + name.Substring(4, 1));
            Fly();
            print(name.Substring(0, 4) + 222);
        }
        else
        {
            print(name.Substring(0, 4));
        }

    }


    void Fly()
    {
        print("fly");
        spoon = GameObject.Find("spoon");
        iTween.RotateAdd(spoon, iTween.Hash("x", 110, "time", 0.5f, "easetype", iTween.EaseType.easeInQuint));
        iTween.RotateAdd(spoon, iTween.Hash("x", -110, "time", 0.8f, "delay", 0.6f, "easetype", iTween.EaseType.easeInQuint));
        iTween.MoveTo(gameObject, iTween.Hash("y", 3.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInQuint));
        iTween.MoveTo(gameObject, iTween.Hash("x", crack.transform.position.x/2, "y", crack.transform.position.y+4, "time", 0.2f, "delay", 0.5f, "easetype", iTween.EaseType.easeOutSine));
        iTween.MoveTo(gameObject, iTween.Hash("x", crack.transform.position.x, "y", crack.transform.position.y, "time", 0.3f, "delay", 0.7f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleTo(gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.5f, "delay", 0.5f, "easetype", iTween.EaseType.easeOutSine));

    }
    public void Stonefly()
    {
        spoon = GameObject.Find("spoon");
        iTween.RotateAdd(spoon, iTween.Hash("x", 40, "time", 0.5f, "easetype", iTween.EaseType.easeInQuint));
        iTween.RotateAdd(spoon, iTween.Hash("x", -40, "time", 0.8f, "delay", 0.6f, "easetype", iTween.EaseType.easeInQuint));
        iTween.MoveTo(gameObject, iTween.Hash("x", -0.59f, "y", 2, "time", 0.2f, "easetype", iTween.EaseType.easeOutSine));
        iTween.MoveTo(gameObject, iTween.Hash("x", -0.59f, "y", -5, "time", 0.2f, "delay", 0.35f, "easetype", iTween.EaseType.easeInSine));
        iTween.ScaleTo(gameObject, iTween.Hash("x", 0.75f, "y", 0.75f, "time", 0.2f, "easetype", iTween.EaseType.easeOutSine));
        iTween.ScaleTo(gameObject, iTween.Hash("x", 0.5f, "y", 0.5f, "time", 0.2f, "delay", 0.35f, "easetype", iTween.EaseType.easeInSine));

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

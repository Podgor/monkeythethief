﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fall : MonoBehaviour
{
    public GameObject well;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void FallingDown()
    {

      
        iTween.MoveTo(gameObject, iTween.Hash("y", well.transform.position.y, "time", 0.3f,  "easetype", iTween.EaseType.easeInSine));
       
        iTween.ScaleAdd(gameObject, iTween.Hash("y", 0.1f, "time", 0.1f));
        iTween.ScaleAdd(gameObject, iTween.Hash("y", -0.1f, "time", 0.1f, "delay", 0.3f));

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
    GameObject zeus,crack;
    
    public void Zeuscrash()
    {
        zeus = GameObject.Find("zeus");
        crack = zeus.transform.Find("crack").gameObject;
        transform.SetParent(zeus.transform);
        iTween.ShakePosition(gameObject, iTween.Hash("y", 0.3f, "time", 0.8f));
        iTween.ShakeScale(gameObject, iTween.Hash("y", 0.3f, "time", 0.8f));
        iTween.ShakePosition(zeus, iTween.Hash("x", 0.5f, "y", 0.5f, "time", 2f, "delay", 1));
        iTween.MoveTo(crack, iTween.Hash("y", 1.84f, "time", 0.2f,"delay", 2, "easetype", iTween.EaseType.easeOutSine));
        iTween.MoveTo(zeus, iTween.Hash("y", -3, "time", 0.3f, "delay", 2.8f, "easetype", iTween.EaseType.easeOutSine));

    }
    public void Zeusshake()
    {
        zeus = GameObject.Find("zeus");
        crack = zeus.transform.Find("crack").gameObject;
        transform.SetParent(zeus.transform);
        iTween.ShakePosition(gameObject, iTween.Hash("y", 0.3f, "time", 0.8f));
        //iTween.ShakeScale(gameObject, iTween.Hash("y", 0.3f, "time", 0.8f));
        iTween.ShakePosition(zeus, iTween.Hash("x", 0.5f, "y", 0.5f, "time", 2f, "delay", 0.3f));
       // iTween.ScaleTo(gameObject, iTween.Hash("x", 0.5f, "y", 0.3f, "time", 0.01f, "delay", 2f));

    }

}

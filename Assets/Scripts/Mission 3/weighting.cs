﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weighting : MonoBehaviour
{

    public GameObject dish_b, dish_w;
    public GameObject blackgate, whitegate;
    public GameObject mask;
    bool a ;


    void Start()
    {
        iTween.RotateAdd(gameObject, iTween.Hash("z", 30, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));
        iTween.RotateAdd(dish_w, iTween.Hash("z", -30, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));
        iTween.RotateAdd(dish_b, iTween.Hash("z", -30, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));
        iTween.MoveTo(blackgate, iTween.Hash("x", blackgate.transform.position.x - 2, "time", 1, "easetype", iTween.EaseType.easeOutQuint));

    }

     public void Wplus()
    {
        iTween.RotateAdd(gameObject, iTween.Hash("z", -30, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Maskcheck"));
        iTween.RotateAdd(dish_w, iTween.Hash("z", 30, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));
        iTween.RotateAdd(dish_b, iTween.Hash("z", 30, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "onComplete", "Jumpout"));


    }
   
    void Maskcheck()
    {
        if (!mask.activeSelf)
        {
            Stoneout();
        }
        else
        {
            Gates();
        }
    }


        
    void Stoneout()
    {
        iTween.ShakePosition(gameObject, iTween.Hash("x", 0.3f, "time", 0.8f, "delay", 0.1f));
        iTween.RotateAdd(gameObject, iTween.Hash("z", 30, "time", 0.5f, "delay", 1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.RotateAdd(dish_w, iTween.Hash("z", -30, "time", 0.5f, "delay", 1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.RotateAdd(dish_b, iTween.Hash("z", -30, "time", 0.5f, "delay", 1f, "easetype", iTween.EaseType.easeInOutSine));

    }



    public void Gates()

    {
        
        if (!a)
        {
            iTween.MoveTo(blackgate, iTween.Hash("x", blackgate.transform.position.x + 2, "time", 1, "easetype", iTween.EaseType.easeOutQuint));
            a = true;
        }
        else
        {
            iTween.MoveTo(whitegate, iTween.Hash("x", whitegate.transform.position.x - 2, "time", 1, "easetype", iTween.EaseType.easeOutQuint));

        }

    }
}

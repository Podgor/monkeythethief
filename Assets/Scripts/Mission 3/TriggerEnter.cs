﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnter : MonoBehaviour
{


    public void OnTriggerEnter2D(Collider2D collision)
    {

        GameObject.Find("monkey").GetComponent<SpriteRenderer>().color = new Color32(155, 155, 155, 255);
        GameObject.Find("statue").transform.position = new Vector3(20, 20, 0);
        GameObject.Find("monkey_shadow").SetActive(false);
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Santaiscoming : MonoBehaviour
{
    public GameObject nightsky;
    public GameObject monkeybody;

    void Start()
    {

        iTween.FadeTo(nightsky, iTween.Hash("alpha", 0, "time", 0));


    }


    public void Night()
    {

        nightsky.GetComponent<SpriteRenderer>().sortingOrder = 20;
        iTween.FadeTo(nightsky, iTween.Hash("alpha", 0.5f, "time", 2, "delay", 0.3f, "onComplete", "Santa", "onCompleteTarget", gameObject));

    }

    void Santa()
    {

        iTween.MoveTo(gameObject, iTween.Hash("x", 0, "time", 2, "easetype", iTween.EaseType.easeOutSine, "onComplete", "Turn"));

    }
    void Turn()
    {

        monkeybody.GetComponent<active_obj>().freeze_y = false;
        GameObject s = GameObject.Find("sack");
        s.transform.SetParent(null);
        iTween.MoveTo(s, iTween.Hash("y", -4, "time", 0.3f, "delay", 0.2f, "easetype", iTween.EaseType.easeOutQuint));
        GetComponent<SpriteRenderer>().flipX = true;
        iTween.MoveTo(gameObject, iTween.Hash("x", 10, "time", 2, "delay", 0.5f, "easetype", iTween.EaseType.easeOutQuint));
        iTween.FadeTo(nightsky, iTween.Hash("alpha", 0, "time", 2, "delay", 3));
    }

    public void Monkeyjump()
    {
        iTween.MoveAdd(monkeybody, iTween.Hash("y", 4, "time", 0.15f, "easetype", iTween.EaseType.easeOutSine));
        iTween.ScaleAdd(monkeybody, iTween.Hash("x", 0.2f, "y", 0.2f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.MoveAdd(monkeybody, iTween.Hash("y", -6, "time", 0.3f, "delay", 0.2f, "easetype", iTween.EaseType.easeInSine));
    }
   
    

}

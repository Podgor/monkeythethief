﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawTree : MonoBehaviour
{
    public LineRenderer line;
    public int count = 0;
    private bool isMousePressed;
    public List<Vector3> pointsList;
    private Vector3 mousePos;

    

    struct myLine
    {
        Vector3 StartPoint;
        Vector3 EndPoint;
    };




    void Awake()
    {

        line = GetComponent<LineRenderer>();

        line.positionCount = 0;
        line.numCapVertices = 5;
        line.startWidth = 0.2f;
        line.endWidth = 0.5f;
        line.useWorldSpace = true;
        isMousePressed = false;
        pointsList = new List<Vector3>();

    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            isMousePressed = true;
            pointsList.RemoveRange(0, pointsList.Count);
            
        }

        if (Input.GetMouseButtonUp(0))
        {
            isMousePressed = false;
            line.positionCount = 0;
        }

        if (isMousePressed)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;

            
            if (!pointsList.Contains(mousePos))
            {
                if (GetComponent<colourpaint>().green)
                //if (GetComponent<colourpaint>().green1 || GetComponent<colourpaint>().green2 || GetComponent<colourpaint>().green3 || GetComponent<colourpaint>().red)
                {
                    pointsList.Add(mousePos);
                    line.positionCount = pointsList.Count;
                    line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);

                }

            }

            if (line.positionCount > count)
            {
                GetComponent<colourpaint>().Colorize();
            }
              
            
        }
        
    }

}

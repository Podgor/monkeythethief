﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grow : MonoBehaviour
{
    public GameObject leaf;
    
    Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void Stemgrow()
    {
        iTween.ScaleAdd(gameObject, iTween.Hash("y", 0.6f, "time", 5.0f, "easetype", iTween.EaseType.easeOutSine));
        StartCoroutine(Delay());

    }
    IEnumerator Delay()
    {
        for (int i = 0; i <= 5; i += 1)
        {
            pos = new Vector3(transform.position.x, transform.position.y + i, 1);
            var clone = Instantiate(leaf, pos, Quaternion.identity);
            if (Mathf.Sign(UnityEngine.Random.Range(-1f, 1f)) > 0)
            {
                clone.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                clone.GetComponent<SpriteRenderer>().flipX = false;

            }

            yield return new WaitForSecondsRealtime(0.5f);
        }
    }

}

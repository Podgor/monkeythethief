﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colosshit : MonoBehaviour
{
    public GameObject monkey;
    public GameObject ruines;
    GameObject tube;

    void Start()
    {
        tube = GameObject.Find("tube");
    }
   
    // Start is called before the first frame update
    public void Leghit()
    {
        iTween.MoveAdd(gameObject, iTween.Hash("y", -19, "time", 0.1f, "delay", 0.5f, "easetype", iTween.EaseType.easeOutExpo));
        iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 0.3f, "y", 0.3f, "time", 0.8f, "delay", 0.7f));
        iTween.MoveAdd(monkey, iTween.Hash("y", 2, "time", 0.1f, "delay", 0.8f, "easetype", iTween.EaseType.easeOutSine));
        iTween.ScaleAdd(monkey, iTween.Hash("y", 0.3f, "time", 0.1f, "delay", 0.8f, "easetype", iTween.EaseType.easeOutSine));
        iTween.MoveAdd(monkey, iTween.Hash("y", -1, "time", 0.15f, "delay", 0.9f, "easetype", iTween.EaseType.easeInSine));
        iTween.ScaleAdd(monkey, iTween.Hash("y", -0.3f, "time", 0.15f, "delay", 0.9f, "easetype", iTween.EaseType.easeOutSine));
        iTween.RotateAdd(tube, iTween.Hash("z", 3, "time", 0.15f, "delay", 0.9f, "easetype", iTween.EaseType.easeInSine));

    }


    public void Legcrack()
    {
        GameObject mask = transform.Find("legmask").gameObject;
        GameObject legcrack = transform.Find("legcrack").gameObject;
        iTween.MoveAdd(mask, iTween.Hash("y", 18, "time", 3f, "delay", 0.1f));
        
    }
    public void Legbreak()
    {

        
        tube.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
    }
}

﻿using UnityEngine;
using System.Collections;

public class Puzzle : MonoBehaviour
{

    
    Vector3 empty1;
    Vector3 empty2;
    Vector3 current;
    float x;
    float y;
    GameObject Empty1;
    GameObject Empty2;
    public bool horizont;


    // текущая и пустая клетка, меняются местами
    private void Start()
    {
        Empty1 = GameObject.Find ("Empty1");
        Empty2 = GameObject.Find("Empty2");

    }


    void Update()
    {
        current = transform.position;
        x = current.x;
        y = current.y;
        empty1 = Empty1.transform.position;
        empty2 = Empty2.transform.position;
    }



    void OnMouseDown()
    {
        
        if (horizont)
        {
            //пустышка слева или справа 
            if (x == empty1.x + 1 && y == empty1.y)
            {
                
                transform.position = empty1;
                empty1.x = empty1.x + 2;
                Empty1.transform.position = empty1;

                return;
            }

            if (x == empty1.x - 2 && y == empty1.y)
            {
                
                x = empty1.x - 1;
                transform.position = new Vector3 (x,y,0);
                empty1.x = empty1.x - 2;
                Empty1.transform.position = empty1;

                return;
            }

            if (x == empty2.x + 1 && y == empty2.y)
            {
                
                transform.position = empty2;
                empty2.x = empty2.x + 2;
                Empty2.transform.position = empty2;

                return;
            }

            if (x == empty2.x - 2 && y == empty2.y)
            {
                
                x = empty2.x - 1;
                transform.position = new Vector3(x, y, 0);
                empty2.x = empty2.x - 2;
                Empty2.transform.position = empty2;

                return;
            }

            //пустышка сверху снизу 

            if (y == empty1.y - 1 && x == empty1.x)
            {
                if (y == empty2.y - 1 && x == empty2.x - 1)
                {
                    
                    transform.position = empty1;
                    empty1.y = empty1.y - 1;
                    empty2.y = empty2.y - 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }

            if (y == empty1.y + 1 && x == empty1.x)
            {
                if (y == empty2.y + 1 && x == empty2.x - 1)
                {
                    
                    transform.position = empty1;
                    empty1.y = empty1.y + 1;
                    empty2.y = empty2.y + 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }

            if (y == empty2.y - 1 && x == empty2.x)
            {
                if (y == empty1.y - 1 && x == empty1.x - 1)
                {
                   
                    transform.position = empty2;
                    empty1.y = empty1.y - 1;
                    empty2.y = empty2.y - 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }

            if (y == empty2.y + 1 && x == empty2.x)
            {
                if (y == empty1.y + 1 && x == empty1.x - 1)
                {
                   
                    transform.position = empty2;
                    empty1.y = empty1.y + 1;
                    empty2.y = empty2.y + 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }
        }

        else

        {
            //пустышка выше ниже
            if (y == empty1.y - 1 && x == empty1.x)
            {
                
                transform.position = empty1;
                empty1.y = empty1.y - 2;
                Empty1.transform.position = empty1;

                return;
            }

            if (y == empty1.y + 2 && x == empty1.x)
            {
                
                transform.position = new Vector3(x, empty1.y + 1, 0);
                empty1.y = empty1.y + 2;
                Empty1.transform.position = empty1;

                return;
            }

            if (y == empty2.y - 1 && x == empty2.x)
            {
               
                transform.position = empty2;
                empty2.y = empty2.y - 2;
                Empty2.transform.position = empty2;

                return;
            }

            if (y == empty2.y + 2 && x == empty2.x)
            {
               
                transform.position = new Vector3(x, empty2.y + 1, 0);
                empty2.y = empty2.y + 2;
                Empty2.transform.position = empty2;

                return;
            }

            //пустышка справа слева

            if (x == empty1.x - 1 && y == empty1.y)
            {
                if (x == empty2.x - 1 && y == empty2.y + 1)
                {
                   
                    transform.position = empty1;
                    empty1.x = empty1.x - 1;
                    empty2.x = empty2.x - 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }

            if (x == empty1.x + 1 && y == empty1.y)
            {
                if (x == empty2.x + 1 && y == empty2.y + 1)
                {
                   
                    transform.position = empty1;
                    empty1.x = empty1.x + 1;
                    empty2.x = empty2.x + 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }

            if (x == empty2.x - 1 && y == empty2.y)
            {
                if (x == empty1.x - 1 && y == empty1.y + 1)
                {
                   
                    transform.position = empty2;
                    empty1.x = empty1.x - 1;
                    empty2.x = empty2.x - 1;
                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }

            if (x == empty2.x + 1 && y == empty2.y)
            {
                if (x == empty1.x + 1 && y == empty1.y + 1)
                {
                   
                    transform.position = empty2;
                    empty1.x = empty1.x + 1;
                    empty2.x = empty2.x + 1;

                    Empty1.transform.position = empty1;
                    Empty2.transform.position = empty2;
                    return;
                }
            }
        }



        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class graphic_hint_object : MonoBehaviour
{
    public string dare;

    
    public List<GameObject> target = new List<GameObject>();
    public List<GameObject> chain_condition = new List<GameObject>();
    public List<GameObject> block_condition = new List<GameObject>();
    public bool useful;
    public bool used;
    public bool need_look;


    //public bool Useful()
    //{
    //    if ((chain_condition.Count == 0 || chain_condition.TrueForAll(c => c == null || c.GetComponent<condition>().acted)) && (block_condition.Count == 0 || block_condition.TrueForAll(c => c == null || c.GetComponent<condition>().acted == false))) return true;
    //    else return false;
    //}
    private void Start()
    {
       if ((chain_condition.Count == 0 || chain_condition.TrueForAll(c => c == null || c.GetComponent<condition>().acted)) && (block_condition.Count == 0 || block_condition.TrueForAll(c => c == null || c.GetComponent<condition>().acted == false))) { useful = true;  }
        else useful = false;
        //Once_in_second();
        InvokeRepeating("Hint_check", 1f, 0.5f);
    }

    //void Once_in_second()
    //{
        
    //}
    private void Hint_check()
    {
        if ((chain_condition.Count == 0 || chain_condition.TrueForAll(c => c == null || c.GetComponent<condition>().acted)) && (block_condition.Count == 0 || block_condition.TrueForAll(c => c == null || c.GetComponent<condition>().acted == false))) { useful = true;  }
        else useful = false;
    }

}

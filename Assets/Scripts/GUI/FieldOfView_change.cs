﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView_change : MonoBehaviour
{
    public int x, y;
    // Start is called before the first frame update
    void Start()
    {
        FOV_change("0.0.-350.20.5");
        Destroy(GameObject.Find("front_objects"), 2);
    }

    public void FOV_change(string pos)
    {


        //StartCoroutine(BoardZoomIn(-350,-750));
        //StartCoroutine(BoardCenter(0, -290));
        string[] poses = pos.Split('.');
        x = int.Parse(poses[0]);
        y = int.Parse(poses[1]);
        if (x == 666) x = Boardtasks.x;
        if (y == 666) y = Boardtasks.y - 60;
        
        iTween.MoveTo(gameObject, iTween.Hash("x", x, "y", y, "z", int.Parse(poses[2]), "time", float.Parse(poses[3]) / 10, "delay", float.Parse(poses[4]) / 10, "easetype", iTween.EaseType.easeInOutQuint));
        
    }

}

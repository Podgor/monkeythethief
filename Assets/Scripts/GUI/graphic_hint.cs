﻿using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class graphic_hint : MonoBehaviour
{
    string dare;
   
    GameObject lens, phone, finger;

    GameObject hints;
    bool need_look;
    int angle;
    public Camera texscam, lenscam;

    Canvas the_back;
    public List<GameObject> currenthints = new List<GameObject>();
   

    List<GameObject> daretarget = new List<GameObject>();
    List<GameObject> clone_daretarget = new List<GameObject>();
    public List<Vector3> clone_daretarget_pos = new List<Vector3>();
    
    
    float delta_x;
    float delta_y;
    Vector3 pos;

    private void Start()
    {

        
        hints = GameObject.Find("hints");


        InvokeRepeating("Hint_choice", 0.3f, 0.3f);

    }


  

    void Hint_choice()
    {

        var children = hints.GetComponentsInChildren<Transform>();

        foreach (var child in children)
        {

            if (currenthints.Contains(child.gameObject) && (!child.GetComponent<graphic_hint_object>().useful || child.GetComponent<graphic_hint_object>().used)) { currenthints.Remove(child.gameObject); } //New_Hint_Button.SetActive(true); }
            if (!currenthints.Contains(child.gameObject) && child.tag == "hint" && child.GetComponent<graphic_hint_object>().useful && !child.GetComponent<graphic_hint_object>().used) { currenthints.Add(child.gameObject); } //New_Hint_Button.SetActive(true); }

        }

       
    }



    public void Get_graphic_help()
    {
        Global.stop_anim = true;
        Invoke("ColortoGS", 0.1f);
        dare = currenthints[0].GetComponent<graphic_hint_object>().dare;
        daretarget = currenthints[0].GetComponent<graphic_hint_object>().target;
        need_look = currenthints[0].GetComponent<graphic_hint_object>().need_look;
        switch (dare)
        {
            

            case "Tap":
                Lens_On();
                //print(dare);
                Dare_Tap();
                Invoke("OnComplete", daretarget.Count + 0.3f);
                Invoke("Lens_off", daretarget.Count + 0.3f);
                Invoke("Lenscam_off", daretarget.Count + 1f);
                break;
            case "DoubleTap":
                Lens_On();
                //print(dare);
                Dare_DoubleTap();
                Invoke("OnComplete", daretarget.Count + 0.3f);
                Invoke("Lens_off", daretarget.Count + 0.3f);
                Invoke("Lenscam_off", daretarget.Count + 1f);
                break;
            case "Take":
                Lens_On();
                
                //print(dare);
                Dare_Take();
                Invoke("OnComplete", daretarget.Count);
                Invoke("Lens_off", daretarget.Count);
                Invoke("Lenscam_off", daretarget.Count + 1f);
                break;
            case "Shake":
                Phone_On();
                
                //print(dare);
                Dare_Shake();
                Invoke("OnComplete", 1.7f);
                Invoke("Phone_off", 1.3f);
                break;
            case "Flip90":
                Phone_On();
                
                //print(dare);
                angle = 90;
                Dare_Flip();
                Invoke("OnComplete", 2.4f);
                Invoke("Phone_off", 2.4f);
                break;
            case "Flip180":
                Phone_On();
              
                //print(dare);
                angle = 180;
                Dare_Flip();
                Invoke("OnComplete", 2.4f);
                Invoke("Phone_off", 2.4f);
                break;
            case "Flip270":
                Phone_On();
               
                //print(dare);
                angle = -90;
                Dare_Flip();
                Invoke("OnComplete", 2.4f);
                Invoke("Phone_off", 2.4f);
                break;
            case "Leave":
                Lens_On();
                //print(dare);
                Dare_Leave();
                Invoke("OnComplete", daretarget.Count);
                Invoke("Lens_off", daretarget.Count);
                Invoke("Lenscam_off", daretarget.Count + 1f);
                break;

            case "Watch":
                Lens_On();
                //print(dare);
                Dare_Watch();
                Invoke("OnComplete", daretarget.Count);
                Invoke("Lens_off", daretarget.Count);
                Invoke("Lenscam_off", daretarget.Count + 1f);
                break;

        }


    }

    void Lens_On()
    {
       
        texscam.GetComponent<Tex_to_sprite>().Get_screenshot();

        for (int i = 0; i < daretarget.Count; i++)
        {
            if (daretarget[i].GetComponent<iTweenPath>()) daretarget[i].GetComponent<iTweenPath>().enabled = false;
            clone_daretarget.Add(Instantiate(daretarget[i], daretarget[i].transform.position, daretarget[i].transform.rotation));
            //clone_daretarget_pos.Add(clone_daretarget[i].transform.position);

            Transform clone_pos = clone_daretarget[i].transform.Find("hint_target");
            if (clone_pos)
            {
                
                clone_daretarget_pos.Add(clone_pos.position);

            }
            else
            {
                clone_daretarget_pos.Add(clone_daretarget[i].transform.position);
            }



            if (clone_daretarget[i].GetComponent<SpriteRenderer>())
            {

                clone_daretarget[i].GetComponent<SpriteRenderer>().enabled = true;
                //clone_daretarget_pos.Add(clone_daretarget[i].transform.position + clone_daretarget[i].GetComponent<SpriteRenderer>().sprite.bounds.center);
            }
            var children = clone_daretarget[i].GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {

                child.gameObject.layer = 13;
                if (child.gameObject.GetComponent<SpriteRenderer>())
                {
 
                    child.gameObject.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                    child.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Front_objects";
                }
                if (child.gameObject.GetComponent<SkeletonAnimation>())
                {

                   
                    child.gameObject.GetComponent<SkeletonAnimation>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                    child.gameObject.GetComponent<MeshRenderer>().sortingLayerName = "Front_objects";
                }
            }


            var children2 = daretarget[i].GetComponentsInChildren<Transform>();
            foreach (var child2 in children2)
            {

                child2.gameObject.layer = 0;
            }


            if (daretarget[i].GetComponent<iTweenPath>()) daretarget[i].GetComponent<iTweenPath>().enabled = true;

        }
        


        GameObject lens_temp = Resources.Load<GameObject>("Prefabs/lens");
      
        Vector3 lenspos = clone_daretarget_pos[0];
        if(need_look) iTween.MoveTo(gameObject, iTween.Hash("x", lenspos.x, "y", lenspos.y, "time", 0.7f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
        //if (clone_daretarget[0].GetComponent<BoxCollider2D>())
        //{
        //    float offset = clone_daretarget[0].GetComponent<BoxCollider2D>().offset.y;
        //    lenspos = new Vector3(clone_daretarget[0].transform.position.x, clone_daretarget[0].transform.position.y + offset, 0);
        //}
        //else
        //{
        //    lenspos = new Vector3(clone_daretarget[0].transform.position.x, clone_daretarget[0].transform.position.y, 0);
        //}

        lens = Instantiate(lens_temp, lenspos, Quaternion.identity);
        lens.name = "lens";
        lenscam.enabled = true;
        finger = GameObject.Find("finger");
        iTween.ScaleTo(lens, iTween.Hash("x", 1.5f, "y", 1.5f, "delay", 0.1f, "time", 0.5f, "easetype", iTween.EaseType.easeOutSine));
        for (int i = 0; i < daretarget.Count; i++)
        {
            var children = daretarget[i].GetComponentsInChildren<Transform>();

            foreach (var child in children)
            {

                child.gameObject.layer = 11;
            }
        }
    }


    void Phone_On()
    {

       
        texscam.GetComponent<Tex_to_sprite>().Get_screenshot();
      
        GameObject phone_temp = Resources.Load<GameObject>("Prefabs/phone");
        pos = GetComponent<Camera>().ScreenToWorldPoint(Vector3.zero);
        phone = Instantiate(phone_temp, Vector3.zero, Quaternion.identity);
        phone.name = "phone";
        Vector2 phone_bounds = phone.GetComponent<BoxCollider2D>().size;
        delta_x = - 2 * pos.x / phone_bounds.x;
        delta_y = - 2 * pos.y / phone_bounds.y;
       
        phone.transform.localScale = new Vector3(delta_x, delta_y, 1);
        lenscam.enabled = true;

        //непонятно, зачем нужна задержка, иначе z = -100? видимо, скриншот не успевает

        Invoke("GetParent", 0.3f);
        Invoke("ColortoGS", 0.3f);
    }
    void GetParent()
    {
        GameObject screentexture = GameObject.Find("screentexture");
        screentexture.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
        screentexture.transform.position = new Vector3(pos.x, pos.y, 0);
        screentexture.transform.SetParent(phone.transform);
    }

        void ColortoGS()
    {
        Camera.main.GetComponent<BWEffect>().ColortoGS();
    }



    public void Dare_Watch()
    {

         for (int j = 0; j < daretarget.Count; j++)
        {
            iTween.ScaleAdd(clone_daretarget[j], iTween.Hash("x", 0.25f, "y", 0.25f, "delay", 0.6f + j, "time", 0.2f, "easetype", iTween.EaseType.easeInOutSine));

            iTween.ScaleAdd(clone_daretarget[j], iTween.Hash("x", -0.25f, "y", -0.25f, "time", 0.3f, "delay", 1.0f + j, "easetype", iTween.EaseType.easeInOutSine));
            Destroy(clone_daretarget[j], 1.3f + j);


            if (j < daretarget.Count - 1)
            {
                iTween.MoveTo(lens, iTween.Hash("x", clone_daretarget_pos[j + 1].x, "y", clone_daretarget_pos[j + 1].y, "time", 0.5f, "delay", 1.2f + j, "easetype", iTween.EaseType.easeInOutSine));
                if (need_look) iTween.MoveTo(gameObject, iTween.Hash("x", clone_daretarget_pos[j + 1].x, "y", clone_daretarget_pos[j + 1].y, "time", 0.7f, "delay", 1.2f, "easetype", iTween.EaseType.easeInOutSine));
            }

            
        }


    }




    public void Dare_Tap()
    {
        
        iTween.MoveTo(finger, iTween.Hash("x", lens.transform.position.x, "y", lens.transform.position.y, "time", 0.2f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        iTween.ScaleAdd(finger, iTween.Hash("x", -0.3f, "y", -0.3f, "delay", 0.8f, "time", 0.1f, "easetype", iTween.EaseType.easeInSine));
        iTween.ScaleAdd(finger, iTween.Hash("x", 0.3f, "y", 0.3f, "delay", 0.9f, "time", 0.2f, "easetype", iTween.EaseType.easeOutSine));

        iTween.ScaleAdd(clone_daretarget[0], iTween.Hash("x", 0.2f, "y", 0.2f, "delay", 0.8f, "time", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleAdd(clone_daretarget[0], iTween.Hash("x", -0.2f, "y", -0.2f, "delay", 0.9f, "time", 0.3f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.MoveAdd(finger, iTween.Hash("x", 3, "y", - 5, "time", 0.3f, "delay", 1.1f, "easetype", iTween.EaseType.easeInOutSine));

        Destroy(clone_daretarget[0], 1.1f );

    }

    public void Dare_DoubleTap()
    {
        
        iTween.MoveTo(finger, iTween.Hash("x", lens.transform.position.x, "y", lens.transform.position.y, "time", 0.2f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        iTween.ScaleAdd(finger, iTween.Hash("x", -0.3f, "y", -0.3f, "delay", 0.8f, "time", 0.1f, "easetype", iTween.EaseType.easeInSine));
        iTween.ScaleAdd(finger, iTween.Hash("x", 0.4f, "y", 0.4f, "delay", 0.95f, "time", 0.1f, "easetype", iTween.EaseType.easeOutSine));
        iTween.ScaleAdd(finger, iTween.Hash("x", -0.4f, "y", -0.4f, "delay", 1.05f, "time", 0.1f, "easetype", iTween.EaseType.easeInSine));
        iTween.ScaleAdd(finger, iTween.Hash("x", 0.3f, "y", 0.3f, "delay", 1.2f, "time", 0.1f, "easetype", iTween.EaseType.easeOutSine));

        iTween.ScaleAdd(clone_daretarget[0], iTween.Hash("x", 0.2f, "y", 0.2f, "delay", 0.8f, "time", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleAdd(clone_daretarget[0], iTween.Hash("x", -0.2f, "y", -0.2f, "delay", 0.9f, "time", 0.2f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleAdd(clone_daretarget[0], iTween.Hash("x", 0.2f, "y", 0.2f, "delay", 1.0f, "time", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleAdd(clone_daretarget[0], iTween.Hash("x", -0.2f, "y", -0.2f, "delay", 1.1f, "time", 0.2f, "easetype", iTween.EaseType.easeInOutSine));

        iTween.MoveAdd(finger, iTween.Hash("x", 3, "y", -5, "time", 0.3f, "delay", 1.2f, "easetype", iTween.EaseType.easeInOutSine));

        Destroy(clone_daretarget[0], 1.4f);
    }



    public void Dare_Leave()
    {
      
   
        for (int j = 0; j < daretarget.Count; j++)
        {
            iTween.ScaleTo(clone_daretarget[j], iTween.Hash("x", daretarget[j].transform.localScale.x * 1.25f, "y", daretarget[j].transform.localScale.y * 1.25f, "delay", 0.6f + j, "time", 0.2f, "easetype", iTween.EaseType.easeInOutSine));
           
            iTween.MoveAdd(clone_daretarget[j], iTween.Hash("x", Mathf.Sign(Random.Range(-1f, 1f)) * 4, "y", Mathf.Sign(Random.Range(-1f, 1f)) * 4, "time", 0.4f, "delay", 0.7f + j, "easetype", iTween.EaseType.easeInOutSine));
            iTween.ScaleTo(clone_daretarget[j], iTween.Hash("x", 0, "y", 0, "time", 0.3f, "delay", 1.0f + j, "easetype", iTween.EaseType.easeInOutSine));
            Destroy(clone_daretarget[j], 1.3f + j);


            if (j < daretarget.Count - 1)
            {
                iTween.MoveTo(lens, iTween.Hash("x", clone_daretarget_pos[j + 1].x, "y", clone_daretarget_pos[j + 1].y, "time", 0.5f, "delay", 1.2f + j, "easetype", iTween.EaseType.easeInOutSine));

                if (need_look) iTween.MoveTo(gameObject, iTween.Hash("x", clone_daretarget_pos[j + 1].x, "y", clone_daretarget_pos[j + 1].y, "time", 0.5f, "delay", 1.2f + j, "easetype", iTween.EaseType.easeInOutSine));
            }

            //print(clone_daretarget.Count);
        }

      
    }


    public void Dare_Take()
    {

        iTween.ScaleTo(clone_daretarget[0], iTween.Hash("x", clone_daretarget[0].transform.localScale.x * 1.25f, "y", clone_daretarget[0].transform.localScale.y * 1.25f, "delay", 0.6f, "time", 0.2f, "easetype", iTween.EaseType.easeInOutSine));
        for (int i = 1; i < daretarget.Count; i++)
        {
            iTween.MoveTo(clone_daretarget[0], iTween.Hash("x", clone_daretarget_pos[i].x, "y", clone_daretarget_pos[i].y, "time", 0.7f, "delay", 0.8f + i-1, "easetype", iTween.EaseType.easeInOutSine));
            iTween.MoveTo(lens, iTween.Hash("x", clone_daretarget_pos[i].x, "y", clone_daretarget_pos[i].y, "time", 0.7f, "delay", 0.8f + i-1, "easetype", iTween.EaseType.easeInOutSine));
            if (need_look) iTween.MoveTo(gameObject, iTween.Hash("x", clone_daretarget_pos[i].x, "y", clone_daretarget_pos[i].y, "time", 0.7f, "delay", 0.8f + i - 1, "easetype", iTween.EaseType.easeInOutSine));

            if (i== daretarget.Count-1) iTween.ScaleTo(clone_daretarget[0], iTween.Hash("x", 0, "y", 0, "time", 0.3f, "delay", 1.5f + i - 1, "easetype", iTween.EaseType.easeInOutSine));
            
        }
        for (int j = 0; j < daretarget.Count; j++)
        {
            Destroy(clone_daretarget[j], daretarget.Count*1.8f);
        }
    }

    public void Dare_Shake()
    {
       
        iTween.ScaleAdd(phone, iTween.Hash("x", -0.7f, "y", -0.7f, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeOutSine));
        iTween.ShakePosition(phone, iTween.Hash("x", 1f, "y", 1f, "time", 0.8f, "delay", 0.8f));


    }

    public void Dare_Flip()
    {
        iTween.ScaleAdd(phone, iTween.Hash("x", -0.7f, "y", -0.7f, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeOutSine));
        iTween.RotateAdd(phone, iTween.Hash("z", angle, "time", 0.8f, "delay", 0.9f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.RotateAdd(phone, iTween.Hash("z", -angle, "time", 0.8f, "delay", 1.7f, "easetype", iTween.EaseType.easeInOutSine));

    }

    
    
    void OnComplete()
    {

        currenthints[0].GetComponent<graphic_hint_object>().used = true;
        currenthints.Remove(currenthints[0]);
        Camera.main.GetComponent<BWEffect>().GStoColor();
           
    }

    void Lens_off()
    {
        if (need_look) iTween.MoveTo(gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.7f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleTo(lens, iTween.Hash("x", 20f, "y", 20f, "delay", 0.1f, "time", 0.7f, "easetype", iTween.EaseType.easeInSine));
        clone_daretarget.Clear();
        clone_daretarget_pos.Clear();
        Destroy(lens, 0.8f);
        Destroy(GameObject.Find("screentexture"), 0.8f);



        


    }

    void Phone_off()
    {
        
        iTween.ScaleTo(phone, iTween.Hash("x", delta_x, "y", delta_y, "time", 0.5f, "easetype", iTween.EaseType.easeOutSine));
        
        var children = phone.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
                      
            if (child.gameObject.GetComponent<SpriteRenderer>())
            {
                iTween.FadeTo(child.gameObject, iTween.Hash("alpha", 0, "time", 0.5f, "delay", 0.4f));
            }
        }
        Destroy(phone, 1f);
       

    }

    void Lenscam_off()
    {
        lenscam.enabled = false;
        Global.stop_anim = false;
    }








}
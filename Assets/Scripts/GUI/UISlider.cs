﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public sealed class UISlider : UIBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    
    public RectTransform Panel;
    private RectTransform canvasRect, rectTransform;
    public float fillHeight, imageHeight, minPosY, maxPosY, targetPosY;
    private bool isDragging, isOpen;



    //public void Open() { if (!this.isOpen && !this.isDragging) this.isOpen = true; }
    //public void Close() { if (this.isOpen && !this.isDragging) this.isOpen = false; }

    protected override void Start()
    {
        Canvas canvas = GetComponentInParent<Canvas>();
        canvasRect = canvas.transform as RectTransform;
        rectTransform = transform as RectTransform;
        imageHeight = rectTransform.sizeDelta.y;
        Vector3[] fillCorners = new Vector3[4];
        Panel.GetLocalCorners(fillCorners);
        fillHeight = Mathf.Abs(fillCorners[0].y * 2f);
        maxPosY = fillCorners[0].y + imageHeight / 2f;
        //minPosY = -240;
        minPosY = fillCorners[1].y - imageHeight / 2f;
    }

    //private void Update()
    //{
    //    UpdateFill();
    //    UpdatePosition();
    //}


    //private void UpdatePosition()
    //{
    //    Vector2 currentPos = this.rectTransform.localPosition;
    //    if (!this.isDragging)
    //    {

    //        float newYPos = (isOpen) ? maxPosY : minPosY;
    //        float speed = Time.deltaTime * 10f;
    //        targetPosY = Mathf.Lerp(currentPos.y, newYPos, speed);
    //    }

    
    //    float yPos = Mathf.Clamp(this.targetPosY, this.maxPosY, this.minPosY);
    //    this.rectTransform.localPosition = new Vector2(currentPos.x, yPos);
    //}

    public void OnBeginDrag(PointerEventData eventData) { this.isDragging = true; }
    public void OnDrag(PointerEventData eventData)
    {
        Camera eventCam = eventData.pressEventCamera;
        Vector2 worldPoint = eventCam.ScreenToWorldPoint(eventData.position);
        Vector2 localPoint = this.canvasRect.InverseTransformPoint(worldPoint);
        targetPosY = localPoint.y;
    }
    public void OnEndDrag(PointerEventData eventData) { this.isDragging = false; }



    private void UpdateFill()
    {
        float value = GetFillValue();
        float newSizeY = this.fillHeight * value;
        RectTransform.Edge edge = RectTransform.Edge.Top;
        Panel.SetInsetAndSizeFromParentEdge(edge, 0f, newSizeY);
    }

    private float GetFillValue()
        {
            float currentYPos = this.rectTransform.localPosition.y;
            float diff = currentYPos - this.minPosY;
            float result = -(diff / (this.fillHeight - this.imageHeight));
            return result;
        }

}

    

    



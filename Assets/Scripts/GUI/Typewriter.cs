﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Typewriter : MonoBehaviour
{
    
    public string gamename;
    // Start is called before the first frame update
    void Start()
    {

        
       // GetComponent<Text>().fontSize = (int)(140 / Global.screenheight);
        StartCoroutine(Delay1());
      
        
    }

    IEnumerator Delay1()
    {
        yield return new WaitForSecondsRealtime(2f);
        StartCoroutine(Delay2());
    }

    IEnumerator Delay2()
    {
        
        for (int i = 0; i < gamename.Length; i++)
        {
            if (i==6)
            {GameObject.Find("bell").GetComponent<AudioSource>().Play();}
            else
            { GetComponent<AudioSource>().Play(); }
            GetComponent<Text>().text = gamename.Substring(0, i+1);
            yield return new WaitForSecondsRealtime(0.3f);
        }
    }


}

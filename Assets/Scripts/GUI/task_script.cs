﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class task_script : MonoBehaviour
{
    public GameObject task;
    public GameObject level;
    //public string [,] Tasks;
    // Start is called before the first frame update
    void Start()
    {


        int _pack = int.Parse(SceneManager.GetActiveScene().name.Substring(6, 1));
        int _level = int.Parse(SceneManager.GetActiveScene().name.Substring(8, 1));
        level.GetComponent<Text>().text = "Уровень " + _pack + "-" + _level;
        task.GetComponent<Text>().text = Global.Tasks[_pack - 1, _level - 1];
        StartCoroutine(Delay());
    }




    IEnumerator Delay()
    {
        byte j;
        for (j = 255; j >= 5; j -= 1)
        {
            task.GetComponent<Text>().color = new Color32(50, 105, 140, j);
            level.GetComponent<Text>().color = new Color32(255, 255, 255, j);
            yield return new WaitForSeconds(0.01f);

        }

        if(j<6)
        {
            task.GetComponent<Text>().enabled = false;
            level.GetComponent<Text>().enabled = false;
        }
    }
}
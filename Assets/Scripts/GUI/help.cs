﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class help : MonoBehaviour
{
    public GameObject background;
    public GameObject hint_text;
    public GameObject book;
    public bool show = false;
    public int i;
    int _pack;
    int _level;
    Color32 color ;
    

    void Start()
    {
        _pack = int.Parse(SceneManager.GetActiveScene().name.Substring(6, 1));
        _level = int.Parse(SceneManager.GetActiveScene().name.Substring(8, 1));
       
    }

        public void get_help()
    {
        background.GetComponent<Button>().interactable = true;
        background.GetComponent<Image>().enabled = true;
        GetComponent<Button>().interactable = false;
        GetComponent<Image>().enabled = false;
        StartCoroutine(shon());
        Global.get_help(_pack, _level, i, show, hint_text, background, gameObject);
        
    }

    IEnumerator Delay()
    {
        for (byte j = 0; j <= 245; j += 10)
        {
            background.GetComponent<Image>().color = new Color32(255, 255, 255, j);
            hint_text.GetComponent<Text>().color = new Color32(255, 255, 255, j);
            yield return new WaitForSeconds(0.01f);

        }
       // GetComponent<Image>().enabled = false;
       // GameObject.Find("Hint_Button").GetComponent<Image>().enabled = true;
       // GameObject.Find("Restart_Button").GetComponent<Image>().enabled = true;
    }

    public void got_help()
    {
        background.GetComponent<Image>().enabled = false;
        background.GetComponent<Button>().interactable = false;
        GetComponent<Image>().enabled = true;
        GetComponent<Button>().interactable = true;

        
        Global.get_help(_pack, _level, i, show, hint_text, background, gameObject);
        
    }

    public void next()
    {
        
        Global.get_help(_pack, _level, i, show, hint_text, background, gameObject);
    }

    public void previos()
    {
        i-=2;
        Global.get_help(_pack, _level, i, show, hint_text, background, gameObject);
    }

    public void close()
    {
        StartCoroutine(hide());
        var q = GameObject.Find("Hint_Button");
        q.GetComponent<Image>().enabled = true;
        q.GetComponent<Button>().interactable = true;
    }

    IEnumerator shon()
    {
        for (float i = -1370; i<=0; i += 50)
        {
            book.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, i);
            yield return null;
        }
    }
    IEnumerator hide()
    {
        for (i = 0; i>=-1370; i -= 50)
        {
            book.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, i);
            yield return null;
        }
    }

}

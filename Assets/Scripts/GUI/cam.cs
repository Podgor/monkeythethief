﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cam : MonoBehaviour
{
    public GameObject player;
    float y_pos;
    Vector3 point;
    // Start is called before the first frame update
    void Start()
    {
        y_pos = player.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, y_pos, -30f), 6.5f * Time.deltaTime);

        point = Camera.main.WorldToViewportPoint(player.transform.position);
        
        if (point.y < 0f || point.y > 1f)
        {
            y_pos = player.transform.position.y;
        }
    }
}

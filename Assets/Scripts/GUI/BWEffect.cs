﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BWEffect : MonoBehaviour
{

    public float intensity;
    private Material material;

    
    void Start()
    {
        material = new Material(Shader.Find("Hidden/BWDiffuse"));
    }

 
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (intensity == 0)
        {
            Graphics.Blit(source, destination);
            return;
        }

        material.SetFloat("_bwBlend", intensity);
        Graphics.Blit(source, destination, material);
    }

    public void ColortoGS ()
    {
        //GetComponent<Camera>().enabled = false;
        //GetComponent<Camera>().enabled = true;
        StartCoroutine(DelayGS());
        //Debug.Log("сереем");
    }
    IEnumerator DelayGS()
    {
        for (float i = 0; i <= 100; i += 5)
        {
            intensity = i / 100;
            //Debug.Log(intensity);
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }
    public void GStoColor()
    {
        //GetComponent<Camera>().enabled = false;
        //GetComponent<Camera>().enabled = true;
        StartCoroutine(DelayColor());
        //Debug.Log("сереем");
    }
    IEnumerator DelayColor()
    {
        for (float i = 0; i <= 100; i += 5)
        {
            intensity = 1-i / 100;
            //Debug.Log(intensity);
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }
}

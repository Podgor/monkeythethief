﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scaling : MonoBehaviour
{
    
    void Start()
    {
        transform.localScale = new Vector3(0, 0, 1);
        GameObject.Find("FrontColor").GetComponent<turnblack>().FadeIn();
        StartCoroutine(Delay());
    }



    IEnumerator Delay()
    {
        for (float i=255; i>=3; i -= 2)
        {
            transform.localScale = new Vector3(200/(i+1), 200 / (i + 1), 1);
            GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, (byte) (i/2+125));
            
            yield return new WaitForSecondsRealtime(0.01f);

        }
    }
}

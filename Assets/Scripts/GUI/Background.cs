﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Background : MonoBehaviour
{

    

    void Start()
    {

        string level = SceneManager.GetActiveScene().name.Substring(6, 3);
        
        GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/background/scene_" + level);

    }


}

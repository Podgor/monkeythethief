﻿using System.Collections;
using UnityEngine;

public class Tex_to_sprite : MonoBehaviour
{
    
    RenderTexture rTex;
    public int koef = 4;
    Texture2D Tex;

    GameObject screentexture;
    Canvas the_back;

    public void Get_screenshot()
    {
        GetComponent<Camera>().enabled = true;
        if (!the_back)
        {
            Canvas back = Resources.Load<Canvas>("Background_prefab");
            the_back = Instantiate(back, Vector3.zero, Quaternion.identity);
            the_back.name = "lenscam_background";
        }
        else the_back.enabled = true;
        the_back.GetComponent<Canvas>().worldCamera = GetComponent<Camera>();


        rTex = new RenderTexture(Screen.width / koef, Screen.height / koef, 24, RenderTextureFormat.RGB565);
       
        GetComponent<Camera>().cullingMask = 1 << 8 | 1 << 11;
        GetComponent<Camera>().targetTexture = rTex;
        GameObject temp_screentexture = Resources.Load<GameObject>("Prefabs/screentexture");
        screentexture = Instantiate(temp_screentexture, Vector3.zero, Quaternion.identity);
        screentexture.name = "screentexture";
        
        StartCoroutine(ShowScreeshot());

    }

   


    private IEnumerator ShowScreeshot()
    {
       
        Tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        for (int i = 0; i < 9; i++)
        {
            yield return null;
        }
        
        RenderTexture.active = rTex;
        
        Tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        Tex.Apply();
        RenderTexture.active = null;
        var spr = Sprite.Create((Texture2D)Tex, new Rect(0, 0, rTex.width, rTex.height), Vector2.zero);
        screentexture.GetComponent<SpriteRenderer>().sprite = spr;
        float koef2 = (koef * Camera.main.orthographicSize * spr.pixelsPerUnit * 2) / (Screen.height);
        screentexture.transform.localScale = new Vector3(koef2, koef2, 1);

        
        screentexture.transform.position = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(0, 0, 100));

        GetComponent<Camera>().targetTexture = null;
        GetComponent<Camera>().enabled = false;
        yield return null;
    }


}



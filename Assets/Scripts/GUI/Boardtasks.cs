﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boardtasks : MonoBehaviour
{
    public List<Vector3> tasks = new List<Vector3>();

    //public static event EventController.MethodContainer MissionsPlacedEvent;
    Vector3 pos;
    public static int x, y;
    public GameObject rope;

    void Start()
    {
        GameObject menu = GameObject.Find("Canvas_menu");
        

            var children = GetComponentsInChildren<Transform>();
            foreach (var child in children)
        {
            
            if (child.name.Length == 15)
            {
                //pos = child.transform.position;
                pos = child.transform.Find("pin").GetComponent<Transform>().position;

                
                if (child.name.Substring(0, 8) == "faketask") { tasks.Add(pos); print("fake " + pos.x + "  " + pos.y + "   " + pos.z); }
                    
                
                if (child.name.Substring(0, 7) == "mission")
                {

                    int name = int.Parse(child.name.Substring(13, 2));
                    Text text = child.transform.Find("text").gameObject.GetComponent<Text>();
                    text.text = "Миссия " + name;


                    Image image = child.transform.Find("image").gameObject.GetComponent<Image>();
                    image.sprite = Resources.Load<Sprite>("Sprites/Missions/mission_" + name);
                    if (name > Global.pack) image.color = new Color32(130, 130, 130, 255);
                    else
                    {
                        Button b = child.gameObject.GetComponent<Button>();
                        b.interactable = true;
                        
                        tasks.Add(pos);
                       print("mission " + pos.x + "  " + pos.y + "   " + pos.z);
                        x = (int)child.position.x;
                        y = (int)child.position.y;
                        
                    }
                   

                }
                SpriteRenderer pin = child.transform.Find("pin").gameObject.GetComponent<SpriteRenderer>();
               
                pin.color = new Color32((byte)(255 + Random.Range(-250, 0)), (byte)(255 + Random.Range(-250, 0)), (byte)(255 + Random.Range(-250, 0)), 255);


            }


        }

    
        rope.GetComponent<rope>().Drawposition();

    }


}

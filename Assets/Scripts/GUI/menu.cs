﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    private void Start()
    {
        if (!PlayerPrefs.HasKey("cur_level"))
        {
            PlayerPrefs.SetInt("cur_level", 0);
            PlayerPrefs.SetInt("pack", 1);
        }

        Global.cur_level = PlayerPrefs.GetInt("cur_level");
        Global.pack = PlayerPrefs.GetInt("pack");
    }

    public void play()
    {
        Global.load_level();
    }

    [ContextMenu("reset")]
    public void resetgame()
    {
        Global.cur_level = 2;
        Global.pack = 1;
        PlayerPrefs.SetInt("cur_level", 2);
        PlayerPrefs.SetInt("pack", 1);
    }
}       

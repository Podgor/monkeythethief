﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orthografic_resize : MonoBehaviour
{

    float w = 1080;
    float h = 1920;
    public float Ratio = 1;
    public const float fixedValue = 8f;
    //public Vector2 nativeRes = new Vector2 (480,800);

    void Awake()
    {
        h = Screen.height;
        w = Screen.width;
        Ratio = w / h;

        GetComponent<Camera>().orthographicSize = fixedValue / Ratio;



    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class go : MonoBehaviour
{
    public GameObject map;
    public GameObject Loose_Button;
    public GameObject Go_Button;
    public GameObject mapbase;
    public Vector3 mapinit;
    public static int remains;
    public GameObject temp;
    public float x;
    public float y;
    public float rot;
    // Start is called before the first frame update
    void Start()
    {
        mapinit = map.transform.localPosition;
        x = mapinit.x;
        y = mapinit.y;
        remains = 3;
    }

    public void f()
    {
        temp.transform.position = Vector3.zero;
    }

    public void Go()
    {
        rot = mapbase.transform.eulerAngles.z;
        remains = remains - 1;
        
        if (remains <=0)
        {
            Invoke("f", 0.5f);

        }

        if (System.Math.Abs(rot-90)<=10)
        {
            x = map.transform.localPosition.x - (float)minus.counter * 0.17f;
        }
        else if (System.Math.Abs(rot - 270) <= 10)
        {
            x = map.transform.localPosition.x + (float)minus.counter * 0.17f;
        }
        else if (System.Math.Abs(rot - 180) <= 10)
        {
            y = map.transform.localPosition.y + (float)minus.counter * 0.168f;
        }

        else
        {
            y = map.transform.localPosition.y - (float)minus.counter * 0.168f;
        }
        
        map.transform.localPosition = new Vector3 (x,y,0);
        minus.counter = 0;
    }

}

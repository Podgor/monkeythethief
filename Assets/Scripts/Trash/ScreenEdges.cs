﻿using UnityEngine;

public class ScreenEdges : MonoBehaviour


{
    public Camera cam; // Камера

    public GameObject movedObj; // объект который нужно двигать
    public GameObject visualObj;

    

    float widthCam; // ширина камеры
    float heightCam;
    Vector2 centrCam; // центр камеры (это ее пивот)
    public float minX, maxX;
    public float minY, maxY;

    
    public Vector3 size;
    public Vector3 a;



    void Start()
    {
        widthCam = cam.orthographicSize * cam.aspect;
        heightCam = cam.orthographicSize;
        centrCam = cam.transform.position;

        minX = centrCam.x - widthCam ;
        maxX = centrCam.x + widthCam ;
        minY = centrCam.y - heightCam ;
        maxY = centrCam.y + heightCam;
        size = visualObj.GetComponent<SpriteRenderer>().bounds.extents;
        // получаем размеры объекта (вернее половины ширины, высоты, глубины)
    }


    void Update()
    {
        float dirX = visualObj.transform.position.x;
        float dirY = visualObj.transform.position.y;
        a = visualObj.transform.position;
        if (Mathf.Abs(dirX) > maxX -1)
        {
            float x = Mathf.Clamp(dirX, minX + size.x, maxX - size.x);

            movedObj.transform.position = new Vector3(x - visualObj.transform.localPosition.x, movedObj.transform.position.y, movedObj.transform.position.z);
        }

        if (Mathf.Abs(dirY) > maxY -1 )
       {
           float y = Mathf.Clamp(dirY, minY + size.y, maxY - size.y);

           movedObj.transform.position = new Vector3(movedObj.transform.position.x, y - visualObj.transform.localPosition.y, movedObj.transform.position.z);
        }
    }


}
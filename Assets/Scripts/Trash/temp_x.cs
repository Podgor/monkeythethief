﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class temp_x : MonoBehaviour
{
    public GameObject point;
    public Vector3 pos1, pos2, size, offset, extents, center, target;


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        center = GetComponent<SpriteRenderer>().sprite.bounds.center;
        size = GetComponent<SpriteRenderer>().sprite.bounds.size;
        extents = GetComponent<SpriteRenderer>().sprite.bounds.extents;
        offset = GetComponent<BoxCollider2D>().offset * Mathf.Cos(transform.localEulerAngles.z);
        target = transform.position - GetComponent<SpriteRenderer>().sprite.bounds.center;

        point.transform.position = target;
        point.transform.localEulerAngles = transform.localEulerAngles;
        point.transform.localScale = new Vector3(0.1f, center.y* transform.localScale.y, 1);
        pos1 = transform.position;
        pos2 = point.transform.position;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vasecount : MonoBehaviour
{
    
    public int [] vasenumber;

    void Start()
    {
        vasenumber = new int[7];
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {

            if (child.tag == "vase")
            {
                int n = int.Parse(child.name.Substring(4, 1));
                vasenumber[n] = n;
            }

        }
    }
}

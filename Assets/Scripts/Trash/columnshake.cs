﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class columnshake : MonoBehaviour
{
   
    public void Columnshake()
    {
        iTween.RotateTo(gameObject, iTween.Hash("z", -10f, "time", 0.1f, "easetype", iTween.EaseType.easeInExpo));
        iTween.RotateTo(gameObject, iTween.Hash("z", 10f, "time", 0.2f, "delay", 0.1f, "easetype", iTween.EaseType.easeInExpo));
        iTween.RotateTo(gameObject, iTween.Hash("z", -7f, "time", 0.1f, "delay", 0.3f, "easetype", iTween.EaseType.easeInExpo));
        iTween.RotateTo(gameObject, iTween.Hash("z", 5f, "time", 0.1f, "delay", 0.4f, "easetype", iTween.EaseType.easeInExpo));
        iTween.RotateTo(gameObject, iTween.Hash("z", 0, "time", 0.05f, "delay", 0.5f, "easetype", iTween.EaseType.easeInExpo));
    }


}

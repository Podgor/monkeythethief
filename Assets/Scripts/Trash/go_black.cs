﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class go_black : MonoBehaviour
{
    // Start is called before the first frame update
    public void toBlack()
    {
        //iTween.ColorTo(gameObject, iTween.Hash("r", 0, "g", 0, "b", 0, "alpha", "1", "delay", 0.1f, "time", 1));
        //iTween.FadeTo(gameObject, 1f, 2.0f);
        iTween.ColorTo(gameObject, new Color32(0, 0, 0, 150), 7);
    }

    public void toTransparent()
    {
        iTween.ColorTo(gameObject, new Color32(255, 255, 255, 0), 3);
    }
}

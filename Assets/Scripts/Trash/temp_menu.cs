﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class temp_menu : MonoBehaviour
{


    Texture2D tex;
    RenderTexture rTex;

    public void Capture()
    //{



    //    string filename = "super_999_screenshot.png";
    //    //string path = Path.Combine(Application.persistentDataPath, filename);
    //    string path = Path.Combine(Application.persistentDataPath, filename);
    //    if (File.Exists(path))
    //        File.Delete(path);
    //    if (Application.platform == RuntimePlatform.IPhonePlayer)
    //        ScreenCapture.CaptureScreenshot(filename);
    //    else
    //        ScreenCapture.CaptureScreenshot(path);
    //    Debug.Log("picture taken " + path);
    //}


    

    {
      
            StartCoroutine(ScreenshotEncode2());
    }

    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        rTex = Resources.Load<RenderTexture>("Texture");
        //RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    IEnumerator ScreenshotEncode2()
    {



        
        yield return new WaitForEndOfFrame();

        byte[] bytes = tex.EncodeToPNG();

        string filename = "super_999_screenshot.png";
        string path = Path.Combine(Application.persistentDataPath, filename);
        
        if (File.Exists(path))
            File.Delete(path);
        if (Application.platform == RuntimePlatform.IPhonePlayer)

            File.WriteAllBytes(filename, bytes);
        else
            File.WriteAllBytes(path, bytes);


        Object.Destroy(tex);

        Debug.Log(path);
    }
    IEnumerator ScreenshotEncode()
    {











        // wait for graphics to render
        yield return new WaitForEndOfFrame();

        // create a texture to pass to encoding
        //RenderTexture rtexture = new RenderTexture(Screen.width / 2, Screen.height /2 ,16);
        
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        // RenderTexture texture = new RenderTexture(Screen.width, Screen.height);
        // put buffer into texture
 
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();
       // GameObject.Find("CameraUI").GetComponent<Camera>().targetTexture = rtexture;

        // split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
        yield return 0;

        byte[] bytes = texture.EncodeToPNG();

        // save our test image (could also upload to WWW)
        
        string filename = "super_999_screenshot.png";
        string path = Path.Combine(Application.persistentDataPath, filename);
        //    string path = Path.Combine(Application.persistentDataPath, filename);
        // Added by Karl. - Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
        if (File.Exists(path))
            File.Delete(path);
        if (Application.platform == RuntimePlatform.IPhonePlayer)

             File.WriteAllBytes(filename, bytes); 
        else
            File.WriteAllBytes(path, bytes);


        Object.Destroy(texture);

        Debug.Log(path);
    }
}


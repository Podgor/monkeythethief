﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere2T : MonoBehaviour
{
    public static event EventController.MethodContainer OnAbroadRight;

    public void TeleportDown()
    {
        transform.Translate(Vector3.down);

        if (transform.position.y < -3) OnAbroadRight();
    }

    public void ResetPosit()
    {
        transform.position = new Vector3(2, 0, 0);
    }
}


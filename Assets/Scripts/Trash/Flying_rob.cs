﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flying_rob : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Fly();
    }

    // Update is called once per frame
    void Fly()
    {
        int i;
        float r = Random.Range(5, 15);
        for (i = 0; i <= 5; i += 1)
        {
            iTween.MoveAdd(gameObject, iTween.Hash("x", Random.Range(-1f, 1f), "y", Random.Range(-1f, 2f), "time", r, "delay", r * i, "easetype", iTween.EaseType.easeInOutSine));
        }
            iTween.MoveTo(gameObject, iTween.Hash("x", 0, "y", -5, "time", Random.Range(10, 20), "delay", r*i, "easetype", iTween.EaseType.easeOutQuint, "onComplete", "Fly"));
    }


    private void OnMouseDown()
    {
        
        iTween.MoveTo(gameObject, iTween.Hash("x", 0, "y", -5, "time", Random.Range(10, 20), "easetype", iTween.EaseType.easeOutQuint));
       
    }
}

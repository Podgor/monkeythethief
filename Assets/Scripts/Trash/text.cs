﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class text : MonoBehaviour
{
  
    string[] row;
    string[,] rows = new string[500, 20];
    List<string> a1, a2, a3 = new List<string>();
    int order;
    int j;



    void Start()
    {
        TextAsset hint_table = Resources.Load<TextAsset>("table2");

        var data_ = hint_table.text.Split(new char[] { '\n' });
        print(data_.Length);

        for (j = 0; j < data_.Length; j++)
        {
            row = data_[j].Split(new char[] { ';' });
            for (int i = 0; i < row.Length-1; i++)
            {
                rows[j, i] = row[i];
            }
            a1.Clear();
            a2.Clear();
            a3.Clear();
            Part1();
        }
    }

    void Part1()
    {

        for (int i = 0; i < row.Length; i++)
        {
                a1.Add(rows[j, i]);
                
            
            if (string.Join(" ", a1).Length > 28)
            {
                a1.Remove(rows[j, i]);
                order = i;
                Part2();
                break;
            }
        }
    }
    void Part2()
    {
       
        for (int i = order; i < row.Length; i++)
        {
            a2.Add(rows[j, i]);
           
            if (string.Join(" ", a2).Length > 28)
            {
                a2.Remove(rows[j, i]);
                
                order = i;
                Part3();
                break;
            }
        }
    }
    void Part3()
    {
        
        for (int i = order; i < row.Length; i++)
        {
            if(rows[j, i] != "") a3.Add(rows[j, i]); 
           
        }
        if (string.Join(" ", a3).Length > 28)
        {
            //print(" 1 " + string.Join(" ", a1) + " 2 " + string.Join(" ", a2) + " 3 " + string.Join(" ", a3));
            //print("   " + string.Join(" ", a1).Length + "   " + string.Join(" ", a2).Length + "   " + string.Join(" ", a3).Length);
            
            string path = "Assets/Resources/test.txt";
                     
            StreamWriter writer = new StreamWriter(path, true);
            writer.WriteLine(" 1 " + string.Join(" ", a1) + " 2 " + string.Join(" ", a2) + " 3 " + string.Join(" ", a3));
            writer.WriteLine("   " + string.Join(" ", a1).Length + "   " + string.Join(" ", a2).Length + "   " + string.Join(" ", a3).Length);
            writer.Close();
        }

    }
}

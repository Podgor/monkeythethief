﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere1T : MonoBehaviour
{
    public static event EventController.MethodContainer OnAbroadLeft;

    public void TeleportUp()
    {
        transform.Translate(Vector3.up);

        if (transform.position.y > 3) OnAbroadLeft();
    }

    public void ResetPosit()
    {
        transform.position = new Vector3(-2, 0, 0);
    }
}

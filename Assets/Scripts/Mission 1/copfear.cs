﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class copfear : MonoBehaviour
{
    
    public GameObject blanket;
   
    public void fear()
    {

        blanket.GetComponent <dream> ().wake = true;
        iTween.RotateTo(gameObject, iTween.Hash("z", - 31.0f, "time", 1.0f, "delay", 0.1f, "easetype", iTween.EaseType.easeInExpo, "OnComplete", "copspritechange"));
        iTween.RotateTo(gameObject, iTween.Hash("z", 0f, "time", 0.5f, "delay", 2.1f, "easetype", iTween.EaseType.easeInExpo, "OnComplete", "blanketspritechange"));
        iTween.MoveAdd(blanket, iTween.Hash("x", -3f, "y", 1.2f, "time", 0.1f, "delay", 2.5f, "OnComplete", "tremor"));
       
    }
    void copspritechange()
    {
         
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Level_1-7/cop_fear");
        return;
    }

    void blanketspritechange()
    {

        blanket.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Level_1-7/blanket2");
        return;
    }



}

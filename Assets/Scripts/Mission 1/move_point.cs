﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class move_point : MonoBehaviour
{






    GameObject back;
    GameObject button;
    GameObject panel;
    Camera todocam;
    GameObject todolist;
    Vector3 pos;



    public bool selected;
    // Start is called before the first frame update
    void Start()
    {

        back = GameObject.Find("back");
        button = GameObject.Find("bacon");
        panel = GameObject.Find("Panel");
        todolist = GameObject.Find("todolist");
        todocam = GameObject.Find("todo_Camera").GetComponent<Camera>();
        pos = todocam.gameObject.transform.position;


    }



    private void OnMouseDown()
    {
        if (back.transform.position == Vector3.zero)
        {
            selected = true;
            if(name == "board")
            {
                GameObject.Find("map").GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
                var children = GetComponentsInChildren<Transform>();
                foreach (var child in children)
                {
                    if (child.gameObject.GetComponent<BoxCollider2D>())
                    { child.gameObject.GetComponent<BoxCollider2D>().enabled = true; }
                }
            }
            back.GetComponent<camera_moving>().scale = 3f;
            back.GetComponent<camera_moving>().target = new Vector3(-transform.position.x* back.GetComponent<camera_moving>().scale, -transform.position.y* back.GetComponent<camera_moving>().scale, 0);
            //iTween.FadeTo(GameObject.Find("rob"), iTween.Hash("alpha", 0, "time", 1)); 
            //iTween.FadeTo(GameObject.Find("border"), iTween.Hash("alpha", 0, "time", 1));
            iTween.FadeTo(GameObject.Find("interier"), iTween.Hash("alpha", 0, "time", 1));
            Color32 fade = new Color32(255, 255, 255, 0);
            GameObject.Find("textlogo").GetComponent<Text>().CrossFadeColor(fade, 1f, false, true);
            GameObject.Find("map").GetComponent<BoxCollider2D>().enabled = true;
            
            button.GetComponent<Image>().enabled = true;
            panel.GetComponent<Image>().enabled = true;
            button.GetComponent<Button>().enabled = true;
            back.GetComponent<camera_moving>().Scale_move();

            if (name == "desk")
            {
                iTween.MoveTo(todocam.gameObject, iTween.Hash("x", todolist.transform.position.x, "y", todolist.transform.position.y, "time", 1.0f, "easetype", iTween.EaseType.easeInOutSine));
                StartCoroutine(TodoResize(1, 320, 500));

            }

        }

        
    }

    public void SizeBack()
    {
        iTween.MoveTo(todocam.gameObject, iTween.Hash("x", pos.x, "y", pos.y, "time", 1.0f, "easetype", iTween.EaseType.easeInOutSine));
        StartCoroutine(TodoResize(-1, 320, 180));

    }

    IEnumerator TodoResize(int coef, int max, int init)
    {
        var t = todocam.orthographicSize;
        for (float i = 0; i <= max; i += 4)
        {
            todocam.orthographicSize = init - coef*i;
            yield return new WaitForSeconds(0.001f);

        }
        yield return new WaitForSeconds(0.001f);

    }
}

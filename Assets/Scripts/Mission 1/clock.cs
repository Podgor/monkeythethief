﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clock : MonoBehaviour
{
    public GameObject minutes;
    public GameObject hours;
    public GameObject border;
    public SpriteRenderer fog;
    Vector3 pos;
    float m, mold;
    
    float h;
    void Start()
    {
        pos = transform.position;
    }

    void Update()
    {
        
         m = 360 - minutes.transform.localEulerAngles.z;
        if(!border.activeSelf)
        {
            float h0 = hours.transform.localEulerAngles.z;
            if (m > 0 && m < 70 && mold > 300)
            {
                hours.transform.localEulerAngles = new Vector3(0, 0, h0 - 30);
            }
            if (m > 300 && m < 360 && mold < 70)
            {
               // hours.transform.localEulerAngles = new Vector3(0, 0, h0 + 30);
            }

            mold = m;
           
            if (hours.transform.localEulerAngles.z >150)
            {
               
                transform.position = new Vector3(pos.x, Mathf.Clamp(pos.y - (360 - hours.transform.localEulerAngles.z) / 40, pos.y - GetComponent<SpriteRenderer>().bounds.extents.y*1.1f, pos.y), 0);
                fog.color = new Color32(255, 255, 255, (byte)(360 - hours.transform.localEulerAngles.z));
            }

                h = hours.transform.localEulerAngles.z;


        }
       
      
    }
}

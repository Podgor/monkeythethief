﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class todo_list : MonoBehaviour
{
    public GameObject task_todo, underline, cross, elon, preview;
    int count_crossed = 0;
    public int x, y, offset_x1, offset_x2, offset_y1, offset_y2;
    Material rMat;
    List<RenderTexture> textures = new List<RenderTexture>();
    GameObject level_preview;
    GameObject todocam;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(GetComponent<RectTransform>().anchoredPosition);

        Global.import_hints();

        task_todo = Resources.Load<GameObject>("Prefabs/task_todo");
        cross = Resources.Load<GameObject>("Prefabs/cross");
        preview = Resources.Load<GameObject>("Prefabs/RawBox");
        //string level = SceneManager.GetActiveScene().name.Substring(6, 3);
        //GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/background/scene_" + Global.pack +"-"+ Global.cur_level);
        //underline = Resources.Load<GameObject>("Prefabs/underline");

        elon = GameObject.Find("elon_mask"); 

        x = 120;
        y = -400;
        offset_x1 = -500;
        //offset_x2 = ;
        offset_y1 = 100;
        //offset_y2 = ;
        
        rMat = new Material(Shader.Find("Unlit/Transparent"));
        

        if (Global.cur_level == 0 || true)
        {
            int rows = Global.Tasks.GetUpperBound(0) + 1;
            int columns = Global.Tasks.GetUpperBound(1) + 1;

            for (int i = 0; i < columns; i++)
            {
                string task = Global.Tasks[Global.pack - 1, i];

                if (task != null && task != "")
                {
                    GameObject todo = Instantiate(task_todo);
                    level_preview = Instantiate(preview);
                    level_preview.name = "level_preview_" + (i + 1);
                    GameObject crossing = Instantiate(cross);

                    
                    //GameObject under = Instantiate(underline);
                    Vector3 size = GameObject.Find("Canvas_todo").GetComponent<RectTransform>().localScale;
                    todo.transform.localScale = size;
                    level_preview.GetComponent<RectTransform>().sizeDelta = new Vector2((float)Screen.width*200 / (float)Screen.height, 200);
                    level_preview.GetComponent<RectTransform>().localScale = GetComponentInParent<Canvas>().transform.localScale;

                    crossing.transform.localScale = size;

                    todo.transform.SetParent(gameObject.transform);
                    level_preview.transform.SetParent(gameObject.transform);
                    crossing.transform.SetParent(gameObject.transform);

                    //under.transform.SetParent(gameObject.transform);
                    //under.transform.localPosition = new Vector2(0, 900 - 420*i);

                    crossing.transform.localPosition = new Vector2(x + offset_x1, y - 420*i + offset_y1);
                    todo.transform.localPosition = new Vector2(x, y - 420 * i);
                    level_preview.transform.localPosition = new Vector2(x + offset_x1, y - 420 * i + offset_y1);

                    crossing.tag = "cross";
                    todo.GetComponent<Text>().text = (i + 1).ToString() + ". " + task;
                    //level_preview.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Previews/Level_" + Global.pack + "-" + (i + 1).ToString());

                    // //GetComponentsinParent<RectTransform>().scale;
                    // new Vector2(1/Global.screenwidth, 1/Global.screenheight);
                    //todo.GetComponent<Text>().fontSize = (int)(120 / Global.screenheight);
                    RenderTexture rTex = new RenderTexture(Screen.width, Screen.height, 24);
                    level_preview.GetComponent<RawImage>().material = rMat;
                    level_preview.GetComponent<RawImage>().texture = rTex;
                    textures.Add(rTex);
                    //level_preview.transform.localScale = new Vector3(Screen.width / 10, Screen.height / 10, 1);

                    

                }
            }
            
        }
        int t = 1;
        foreach (GameObject G in GameObject.FindGameObjectsWithTag("cross"))
        {
            if (t < Global.cur_level)
            {
                G.GetComponent<Image>().fillAmount = 1;
            }
            t++;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    int count_crossed_tasks()
    {
        int i = 0;
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("cross"))
        {
            if (g.GetComponent<Image>().fillAmount == 1)
            {
                i++;
            }
        }
        return i;
    }

    public void cross_out()
    {
        print("cr");
        if (count_crossed_tasks() == Global.cur_level - 1)
        {
            if (Global.cur_level == 1)
            {
                StartCoroutine(Crossing_out(true, false));
            }
            else
            {
                StartCoroutine(Crossing_out(false, false));
            }
        }

        else
        {
            //StartCoroutine(Async());
            StartCoroutine(Crossing_out(false, true));
        }
    }

    IEnumerator Crossing_out(bool first, bool remove)
    {
        var local_this = GetComponent<RectTransform>();
        
        var elon_local = elon.GetComponent<RectTransform>();
        GetComponent<RectTransform>().anchoredPosition = new Vector2(GetComponent<RectTransform>().anchoredPosition.x, -2050 + 420*(Global.cur_level-2));
        if (true)
        {
            AsyncOperation asy = SceneManager.LoadSceneAsync("level_" + Global.pack + "-" + Global.cur_level);
            while (!asy.isDone)
            {
                yield return null;
            }
            todocam = GameObject.Find("todo_Camera");
            todocam.GetComponent<Camera>().enabled = true;
            Camera.main.targetTexture = textures[Global.cur_level - 1];
            Camera.main.GetComponent<BWEffect>().ColortoGS();


            if (first)
            {
                GetComponent<RectTransform>().anchoredPosition = new Vector2(local_this.anchoredPosition.x, -2879);

            }
            for (float i = elon.GetComponent<RectTransform>().anchoredPosition.x; i>=0; i -= 95)
            {
                elon_local.anchoredPosition = new Vector2(i, elon_local.anchoredPosition.y);
                local_this.anchoredPosition = new Vector2(i, local_this.anchoredPosition.y);
                yield return new WaitForSeconds(0);

            }
            elon.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, elon.GetComponent<RectTransform>().anchoredPosition.y);
            GetComponent<RectTransform>().anchoredPosition = new Vector2(0, GetComponent<RectTransform>().anchoredPosition.y);
            //SceneManager.LoadScene();

            foreach (Camera G in FindObjectsOfType<Camera>())
            {
                print(G.name);
            }

            if (remove)
            {
                var gameo = GameObject.FindGameObjectsWithTag("cross")[count_crossed_tasks()];
                for (float j = 0; j<=1; j += 0.05f)
                {
                    gameo.GetComponent<Image>().fillAmount = j;
                    yield return new WaitForSeconds(0.01f);
                }
                gameo.GetComponent<Image>().fillAmount = 1;
            }



            
            if (first)
            {
                for (float i = -2879; i<=-2050; i += 35)
                {
                    local_this.anchoredPosition = new Vector2(local_this.anchoredPosition.x, i);
                    print("awdadw");
                    yield return new WaitForSeconds(0);
                }
                local_this.anchoredPosition = new Vector2(local_this.anchoredPosition.x, - 2050) ;
            }
            else
            {
                //print("goven");
                for (float i = local_this.anchoredPosition.y; i <= -2050 + 420*(count_crossed_tasks()); i += 35)
                {
                    local_this.anchoredPosition = new Vector2(local_this.anchoredPosition.x, i);
                    yield return new WaitForSeconds(0);
                }
                local_this.anchoredPosition = new Vector2(local_this.anchoredPosition.x,  - 2050 + 420*(count_crossed_tasks()));
            }

            yield return new WaitForSeconds(0.8f);

            for (float i = 0; i <= 2050; i +=85)
            {
                elon_local.anchoredPosition = new Vector2(i, elon_local.anchoredPosition.y);
                local_this.anchoredPosition = new Vector2(i, local_this.anchoredPosition.y);
                yield return new WaitForSeconds(0);

            }
            elon.GetComponent<RectTransform>().anchoredPosition = new Vector2(2050, elon.GetComponent<RectTransform>().anchoredPosition.y);
            GetComponent<RectTransform>().anchoredPosition = new Vector2(2050, GetComponent<RectTransform>().anchoredPosition.y);
            
        }
        
    }

    public void StartPlay ()
    {
        
        StartCoroutine(PlaySceneDelay());
        

    }

    IEnumerator PlaySceneDelay()
    {
        yield return new WaitForSeconds(2);
        GameObject current = GameObject.Find("level_preview_" + Global.cur_level);
        
        GameObject paper = GameObject.Find("paper");
        todocam.transform.SetParent (GameObject.Find("Canvas_todo").transform);
       // current.transform.SetParent(GameObject.Find("Canvas_todo").transform);
        //paper.transform.SetParent(current.transform);
        //GameObject.Find("level_preview_2 (1)").transform.SetParent(current.transform);
        //current.GetComponent<RawImage>().enabled = false;
        //GameObject.Find("level_preview_2 (1)").GetComponent<RectTransform>().localScale = Vector3.one;
        
        var jx = (todocam.transform.position.x - current.transform.position.x) * ((float)Screen.height / 800) / (100 );
        var jy = (todocam.transform.position.y - current.transform.position.y) * ((float)Screen.height / 800) / (100);
        var s = Mathf.Abs(todocam.GetComponent<Camera>().orthographicSize - 100*GetComponentInParent<Canvas>().GetComponent<RectTransform>().localScale.y) * ((float)Screen.height / 800) / (100);
        print(Screen.height);
        for (float i = 0; i < 10/((float)Screen.height/800); i += 0.1f)
        {
            //print((i, 10 / ((float)Screen.height / 800)));
            
            //print(Screen.height);
            //двигаем и скейлим саму превьющку норм

            //это и тукану понятно
            //current.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width*i/10, Screen.height*i/10);
            //current.GetComponent<RectTransform>().position = new Vector3(todocam.transform.position.x, todocam.transform.position.y, 0);
            //iTween.MoveTo(todocam, iTween.Hash("x", current.transform.position.x, "y", current.transform.position.y, "time", 0.8f, "easetype", iTween.EaseType.easeOutSine));
            todocam.GetComponent<Camera>().orthographicSize -= s;
            todocam.transform.position -= new Vector3(jx, jy);
            //current.GetComponent<RectTransform>().localScale = new Vector2(i, i);
            Camera.main.GetComponent<BWEffect>().intensity = 1-i/10;
            //print(Camera.main.GetComponent<BWEffect>().intensity);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        Camera.main.targetTexture = null;
        todocam.GetComponent<Camera>().enabled = false;
    }


    //IEnumerator Async()
    //{
    //    AsyncOperation async = SceneManager.LoadSceneAsync("level_" + Global.pack + " - " + Global.cur_level);

    //    while (!async.isDone)
    //    {
    //        yield return null;
    //    }
    //}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shadowmood : MonoBehaviour
{
    GameObject shadow;
    public Transform burger;
    public GameObject Area;
    Vector3 pos;
    
    float coefy;


    private void Start()
    {
        
        pos = new Vector3(transform.position.x + 0.11f, transform.position.y + 0.12f, 0);
        shadow = Instantiate(this.gameObject, pos, Quaternion.identity);
        Destroy(shadow.GetComponent<shadowmood>());
        
        Destroy(shadow.GetComponent<BoxCollider2D>());
        shadow.transform.SetParent(this.gameObject.transform);
        shadow.GetComponent<SpriteRenderer>().color = new Color32(155, 155, 155, 70);
        
    }

    void Update()
    {
        
        coefy = Mathf.Abs(transform.position.y - pos.y)*2 + 1;
        if (transform.position.y - pos.y < 0 && transform.position.y > Area.GetComponent<Area>().minY)
        {
        shadow.transform.localScale = new Vector3(coefy, coefy, 1);
        shadow.transform.position = new Vector3(transform.position.x + 0.11f * coefy * 2 , transform.position.y + 0.12f * coefy * 2, 1);
        }

    }

    public void hunt()
    {
        iTween.MoveTo(gameObject, iTween.Hash("y", burger.position.y, "time", 6f, "delay", 0.7f));

        iTween.MoveTo(gameObject, iTween.Hash("y", burger.position.y + 10f, "time", 6f, "delay", 7.5f));
    }

}

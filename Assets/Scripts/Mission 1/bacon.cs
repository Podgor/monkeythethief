﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bacon : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject back;
    GameObject panel;
    void Start()
    {
        back = GameObject.Find("back");
        panel = GameObject.Find("Panel");
    }

    //public void BackButtonActivate()
    //{
    //    GetComponent<Button>().interactable = true;
    //}

    public void Fullscale()
    {
        back.GetComponent<camera_moving>().target = Vector3.zero;
        back.GetComponent<camera_moving>().scale = 1;
        back.GetComponent<camera_moving>().Scale_move();
        GetComponent<Image>().enabled = false;
        GetComponent<Button>().enabled = false;
        panel.GetComponent<Image>().enabled = false;

        foreach (move_point point in FindObjectsOfType<move_point>())
        {
            point.selected = false;
          
            //iTween.FadeTo(GameObject.Find("rob"), iTween.Hash("alpha", 1, "time", 1));
            iTween.FadeTo(GameObject.Find("interier"), iTween.Hash("alpha", 1, "time", 1));
            //iTween.FadeTo(GameObject.Find("border"), iTween.Hash("alpha", 1, "time", 1));
            GameObject.Find("map").GetComponent<BoxCollider2D>().enabled = false;
            Color32 fade = new Color32(255, 255, 255, 255);
            GameObject.Find("textlogo").GetComponent<Text>().CrossFadeColor(fade, 2f, false, true);
            if (point.name == "board")
            {
                GameObject.Find("map").GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                var children = point.GetComponentsInChildren<Transform>();
                foreach (var child in children)
                {
                    if (child.gameObject.GetComponent<BoxCollider2D>() && child.gameObject.name != "board")
                    { child.gameObject.GetComponent<BoxCollider2D>().enabled = false; }
                }
            }
        }

    }
} 


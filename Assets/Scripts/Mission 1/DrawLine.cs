﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawLine : MonoBehaviour
{
    public LineRenderer line;
    //public SpriteRenderer peninhand;
    private bool isMousePressed;
    public List<Vector3> pointsList;
    private Vector3 mousePos;
    public GameObject AI_Rob, mypen, case_;
    public Scratch scratchScript;

    struct myLine
    {
         Vector3 StartPoint;
         Vector3 EndPoint;
    };


    
    public bool eye_1, eye_2;
    public Transform eye1, eye2;
    public float approx;
    





    void Awake()
    {
        
        line = GetComponent<LineRenderer>();
        
        line.positionCount = 0;
        line.numCapVertices = 5;
        line.startWidth = 0.2f;
        line.endWidth = 0.5f;
        //line.useWorldSpace = true;
        isMousePressed = false;
        pointsList = new List<Vector3>();
       
    }
   
    void Update()
    {
     
        if (Input.GetMouseButtonDown(0) && !isMousePressed)
        {

            Start_Draw();
            
          


        }

        if (Input.GetMouseButtonUp(0) && isMousePressed)
        {
            Stop_Draw();
            eye_1 = false;
            eye_2 = false;
        }
        
        if (isMousePressed)
        {
            mypen.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);
            if ((transform.position - eye1.position).sqrMagnitude < approx)
            {
                eye_1 = true;

            }
            if ((transform.position - eye2.position).sqrMagnitude < approx)
            {
                eye_2 = true;

            }

            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;


            if (!pointsList.Contains(mousePos))
            {
                pointsList.Add(mousePos);

                line.positionCount = pointsList.Count;
                line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
                scratchScript.AssignScreenAsMask();
            }
        }

        if (eye_1 && eye_2)
        {

            Global.yesitwas[2] = true;
            
        }


    }

    void Start_Draw()
    {
        Global.busy = true;
        print("draw");
        isMousePressed = true;
        pointsList.RemoveRange(0, pointsList.Count);
        mypen.GetComponent<SpriteRenderer>().enabled = true;
        AI_Rob.GetComponent<Attach_sprite>().sprite = null;
        AI_Rob.GetComponent<Attach_sprite>().Initialize(true);
        AI_Rob.GetComponent<rob_animation>().sprite_to_take.GetComponent<SpriteRenderer>().enabled = true;
    }
    public void Stop_Draw()
    {
        Global.busy = false;
        print("stop draw");
        isMousePressed = false;
        line.positionCount = 0;
        mypen.GetComponent<SpriteRenderer>().enabled = false;
        AI_Rob.GetComponent<Attach_sprite>().sprite = AI_Rob.GetComponent<rob_animation>().sprite_to_take.GetComponent<SpriteRenderer>().sprite;
        AI_Rob.GetComponent<Attach_sprite>().Initialize(true);
        AI_Rob.GetComponent<rob_animation>().sprite_to_take.GetComponent<SpriteRenderer>().enabled = false;
        scratchScript.AssignScreenAsMask();

    }

    public void Get_case()
    {
        AI_Rob.GetComponent<rob_animation>().sprite_to_take = case_;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moneyshake : MonoBehaviour
{

    public float y;
    public void Shaker()
    {
        iTween.RotateTo(gameObject, iTween.Hash("x", 360f, "time", 1f));
        iTween.ShakePosition(gameObject, iTween.Hash("x", 0.3f, "y", 0.3f, "time", 0.8f, "delay", 0.1f));
        iTween.MoveTo(gameObject, iTween.Hash("y", y, "time", 0.3f, "delay", 1f, "easetype", iTween.EaseType.easeInSine));
    }
}

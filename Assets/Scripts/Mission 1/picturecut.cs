﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class picturecut : MonoBehaviour
{

    public GameObject picture;
    public void Cut()
    {

        Vector3 pos = picture.transform.position;
        Vector3 size = picture.GetComponent<SpriteRenderer>().bounds.extents;
        Vector3 knifesize = GetComponent<SpriteRenderer>().bounds.extents;
        transform.localEulerAngles = new Vector3(0,0,-30);
        iTween.MoveTo(gameObject, iTween.Hash("x", pos.x - size.x - knifesize.x, "y", pos.y + size.y + knifesize.y, "time", 0.5f,  "easetype", iTween.EaseType.easeOutQuad));
        iTween.MoveTo(gameObject, iTween.Hash("y", pos.y - size.y + knifesize.y, "time", 0.5f, "delay", 0.5f, "easetype", iTween.EaseType.easeOutCubic));
        iTween.MoveTo(gameObject, iTween.Hash("x", pos.x + size.x - knifesize.x, "time", 0.5f, "delay", 1.0f, "easetype", iTween.EaseType.easeOutCubic));
        iTween.MoveTo(gameObject, iTween.Hash("y", pos.y + size.y + knifesize.y, "time", 0.5f, "delay", 1.5f, "easetype", iTween.EaseType.easeOutCubic));
        iTween.MoveTo(gameObject, iTween.Hash("x", pos.x - size.x - knifesize.x, "time", 0.5f, "delay", 2.0f, "easetype", iTween.EaseType.easeOutCubic));
    }
}

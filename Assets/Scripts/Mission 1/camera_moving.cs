﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_moving : MonoBehaviour
{
    public Vector3 target;
    public float scale;
    public Camera todocam;
    //public bool move;
    public Vector3 pos, pos1, pos2, pos3;


    // Update is called once per frame
    //void Update()
    //{
    //    if (move)
    //    {
    //        //print("pobedinsky");

    //        //transform.position = Vector2.MoveTowards(transform.position, target, 0.12f);
    //        iTween.MoveTo(gameObject, iTween.Hash("x", target.x, "y", target.y, "time", 1f, "easetype", iTween.EaseType.easeInOutSine));
    //        iTween.ScaleTo(gameObject, iTween.Hash("x", scale, "y", scale, "time", 1f, "easetype", iTween.EaseType.easeInOutSine));
    //        //transform.localScale = Vector3.MoveTowards(transform.localScale, new Vector3(scale, scale,1), 0.01f);

    //        if (System.Math.Abs(transform.position.x-target.x)<0.01 && System.Math.Abs(transform.position.y - target.y) < 0.01)
    //        {
    //            transform.position = target;
    //            move = false;
    //        }
    //    }
    //}
    public void Scale_move()
    {

        pos = transform.position;
        pos1 = todocam.ScreenToWorldPoint(pos);
        pos2 = todocam.ScreenToWorldPoint(target);

        iTween.MoveAdd(todocam.gameObject, iTween.Hash("x", target.x - pos.x, "y", target.y - pos.y, "time", 1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.MoveTo(gameObject, iTween.Hash("x", target.x, "y", target.y, "time", 1f, "easetype", iTween.EaseType.easeInOutSine));
        iTween.ScaleTo(gameObject, iTween.Hash("x", scale, "y", scale, "time", 1f, "easetype", iTween.EaseType.easeInOutSine));
           

            if (System.Math.Abs(transform.position.x - target.x) < 0.01 && System.Math.Abs(transform.position.y - target.y) < 0.01)
            {
                transform.position = target;
            }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scalereset : MonoBehaviour
{
   
    public GameObject Area;
    public GameObject Monk;
    
    bool scaled;
    bool m;

    private void Update()
    {
        m = Monk.activeSelf;
    }

    void OnMouseDown()
    {
        // коррекция размеров кейса при отсоединении в случае скейла родительског объекта
        if (!scaled)
        {
            Area.GetComponent<Area>().scaler =1 / Monk.transform.localScale.x;
            scaled = true;
        }
            
        
      
        //коррекция области перемещения кейса для попадания в автобус
        if (!m)
        {
            Area.GetComponent<Area>().minX = transform.localPosition.x - 3;
            Area.GetComponent<Area>().maxX = transform.localPosition.x + 1;
            Area.transform.localScale = new Vector3(1.1f, 1.1f, 1);
        }

      
    }


}
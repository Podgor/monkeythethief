﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turnred : MonoBehaviour
{
    
    private void Start()
    {
        
        
      
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (!child.gameObject.GetComponent<turnred>())
            {

                if (child.gameObject.GetComponent<SpriteRenderer>())
                {
                    child.gameObject.AddComponent<turnred>();

                }
            }
            else
            {
                if (child.gameObject.name != "Scene")
                {
                    return;

                }
            }
        }
        
    }



    public void Turnred()
    {
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.gameObject.GetComponent<SpriteRenderer>())
            {
                child.GetComponent<SpriteRenderer>().color = new Color32(255, 100, 100, 255);

            }

        } 
    }

    public void Turnwhite()
    {
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.gameObject.GetComponent<SpriteRenderer>())
            {
                child.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);

            }

        }
    }
}

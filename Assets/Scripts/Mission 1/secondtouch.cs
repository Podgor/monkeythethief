﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class secondtouch : MonoBehaviour
{
    
    
    float y0;
    public GameObject knife;
    public GameObject coin;
    public GameObject blade;
    bool touch;
    bool cointouch;
    public bool a1, a2;
    

    void OnMouseDown()
    {
        touch = true;
    }
    void OnMouseUp()
    {
        touch = false;
    }

    void Update()
    {
        if (Global.yesitwas[0])
        {
            cointouch = coin.GetComponent<cointiuch>().cointouch;
            if (Input.touchCount == 2 && (touch || cointouch))
            {


                y0 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position).y;

                Vector3 knifepos = Camera.main.ScreenToWorldPoint(Input.GetTouch(1).position);
                if (knifepos.y > transform.position.y - GetComponent<SpriteRenderer>().bounds.extents.y && knifepos.y < transform.position.y + GetComponent<SpriteRenderer>().bounds.extents.y)
                {
                    coin.transform.position = new Vector3(0.1f, -3.1f, 0);
                    knife.transform.position = new Vector3(knifepos.x, knifepos.y, 0);

                    if (knifepos.x > transform.position.x - GetComponent<SpriteRenderer>().bounds.extents.x && knifepos.x < transform.position.x)
                    { a1 = true; }
                    if (knifepos.x > transform.position.x && knifepos.x < transform.position.x + GetComponent<SpriteRenderer>().bounds.extents.x)
                    { a2 = true; }
                    if (a1 && a2)
                    { blade.transform.position = new Vector3(0, 0, 0); }
                }
            }
        }
    }
}

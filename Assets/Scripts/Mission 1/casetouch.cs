﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class casetouch : MonoBehaviour
{
    public GameObject caseinhand;
  
    public GameObject casealone;
    public GameObject monkwcase;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 m = monkwcase.transform.position;
        Vector3 c = casealone.transform.position;
        if (Mathf.Abs(m.x - c.x) < 1 && Mathf.Abs(m.y - c.y) < 1)
        {
            caseinhand.SetActive(true);
            
            casealone.SetActive(false);
        }
    }
}
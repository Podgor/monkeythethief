﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class item_pickup : MonoBehaviour
{
    public GameObject Rob;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnMouseDown()
    {
        if(!Rob.GetComponent<rob_animation>().attached)
        {
            Rob.GetComponent<rob_animation>().sprite_to_take = this.gameObject;
            Rob.GetComponent<Attach_sprite>().sprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
            Rob.GetComponent<rob_animation>().Pickup();
            
        }

    }
}

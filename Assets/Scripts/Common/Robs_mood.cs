﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robs_mood : MonoBehaviour
{





    public void LeftHandTake()
    {
        GameObject.Find("hand_left").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Rob/Front/hand"); 
    }
    public void RightHandTake()
    {
        GameObject.Find("hand_right").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Rob/Front/hand");
    }
    public void LeftHandPut()
    {
        GameObject.Find("hand_left").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Rob/Front/open_hand");
    }
    public void RightHandPut()
    {
        GameObject.Find("hand_right").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Rob/Front/open_hand");
    }
    public void Happy()
    {
        transform.Find("mouth").gameObject.GetComponent<SpriteRenderer>().flipY = false;
    }
    public void Unhappy()
    {
        transform.Find("mouth").gameObject.GetComponent<SpriteRenderer>().flipY = true;
    }
}

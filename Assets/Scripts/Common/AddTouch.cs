﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTouch : MonoBehaviour
{

    void Start()
    {
        var children = GameObject.Find("Scene").GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if ((child.gameObject.GetComponent<BoxCollider2D>() || child.gameObject.GetComponent<PolygonCollider2D>() || child.gameObject.GetComponent<CircleCollider2D>())&& child.gameObject.GetComponent<SpriteRenderer>())
            {
                
                child.gameObject.AddComponent<TouchEffect>();

            }
        }
    }

}

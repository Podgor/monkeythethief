﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orderset : MonoBehaviour
{

    public GameObject Scene;
    public GameObject monkey;
    public GameObject mask;
    

    List<int> list = new List<int>();

    public void OnMouseDrag()
    {
        
        var children = Scene.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
           
            if (child.gameObject.GetComponent<SpriteRenderer>())
            {
                float yi = child.gameObject.transform.position.y ;
                float ym = transform.position.y ;
               

                if (ym < yi)
                {
                     list.Add(child.gameObject.GetComponent<SpriteRenderer>().sortingOrder);
                    
                }

                
            }

               
        }
       
        mask.GetComponent<SpriteRenderer>().sortingOrder = Mathf.Max(list.ToArray()) + 2;
        monkey.GetComponent<SpriteRenderer>().sortingOrder = Mathf.Max(list.ToArray()) +1;
        list.Clear();
    }
}

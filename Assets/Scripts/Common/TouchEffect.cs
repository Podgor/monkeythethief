﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchEffect : MonoBehaviour
{
    public List<Color32> colors = new List<Color32>();
    
    
    void OnMouseDown()
    {
        if (tag == "rocky") StartCoroutine("Rock");

        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.gameObject.GetComponent<SpriteRenderer>())
            {
                colors.Add(child.gameObject.GetComponent<SpriteRenderer>().color);
                child.gameObject.GetComponent<SpriteRenderer>().color = new Color32(240, 240, 240, 245);

            }
        }
    }


    void OnMouseUp()
    {
        var children = GetComponentsInChildren<Transform>();
        int i = 0;
        foreach (var child in children)
        {
            if (child.gameObject.GetComponent<SpriteRenderer>())
            {

                child.gameObject.GetComponent<SpriteRenderer>().color = colors[i];
                i++;
               
                

            }
        }
        if (colors.Count > 0) colors.Clear();
    }


    public IEnumerator Rock()
    {
        float angle = 30, t = 0.7f, w = 5;
        print("let's rock!!!");
        GetComponent<BoxCollider2D>().enabled = false;
        iTween.RotateAdd(gameObject, iTween.Hash("z", angle, "time", t, "easetype", iTween.EaseType.easeInOutSine));

        yield return new WaitForSeconds(t);

        float deltaZ = 0;
        float Zold = angle;
        int direction = -1;
        for (int i = 1; i < 10; i++)
        {
            
            deltaZ= angle * direction * Mathf.Exp(-2*i/w);
            
                iTween.RotateAdd(gameObject, iTween.Hash("z", deltaZ - Zold, "time", 2*t, "easetype", iTween.EaseType.easeInOutSine));
                yield return new WaitForSeconds(2*t);
                // print(amp);
                direction = -direction;
                Zold = deltaZ;
         
            
        }
        iTween.RotateTo(gameObject, iTween.Hash("z", 0, "time", t, "easetype", iTween.EaseType.easeInOutSine));
        GetComponent<BoxCollider2D>().enabled = true;



    }
}
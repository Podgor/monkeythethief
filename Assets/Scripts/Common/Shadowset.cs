﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadowset : MonoBehaviour
{
    
    void Start()
    {
        var rays = GameObject.FindGameObjectsWithTag("Shadowed");
       

        foreach (GameObject ray in rays)
        {
            var clone = Instantiate(ray);
            clone.transform.SetParent(ray.transform);
            clone.transform.localPosition = new Vector3(0, 0.1f, 0);
            clone.transform.localScale = new Vector3(1, 0.4f, 1);
            clone.AddComponent<Shadowcast>();
            clone.GetComponent<SpriteRenderer>().flipY = true;
            clone.GetComponent<SpriteRenderer>().color = new Color32(0, 0, 0, 100);
            clone.GetComponent<SpriteRenderer>().sortingOrder = 2;
            clone.name = ray.name + "_shadow";
            if (clone.gameObject.GetComponent<BoxCollider2D>())
            {
                clone.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }




























           // clone.tag = "shadow";
        }
        
    }

   
}

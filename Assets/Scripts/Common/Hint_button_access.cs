﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hint_button_access : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Access()
    {
        Camera.main.GetComponent<graphic_hint>().Get_graphic_help();
    }

    void Update()
    {
        if (Camera.main.GetComponent<graphic_hint>().currenthints.Count == 0) GetComponent<Button>().interactable = false;
        else GetComponent<Button>().interactable = true;
    }
}

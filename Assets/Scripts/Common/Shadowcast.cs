﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadowcast : MonoBehaviour
{

    GameObject sun;
    float y1;
    Vector3 scale;


    void Start()
    {
         sun = GameObject.Find("sun");
         y1 = sun.transform.position.y;
        scale  = transform.localScale;
    }

    void Update()
    {
  
        float x = sun.transform.position.x - transform.position.x;
        float y = sun.transform.position.y;
        float Angle;

        if (sun.transform.position.x > transform.position.x)
        {
            Angle = Mathf.Atan(y / x) * 57.2956f - 90;
        }
        else
        {
            Angle = Mathf.Atan(y / x) * 57.2956f + 90;
        }
        
        transform.localEulerAngles = new Vector3(0, 0, Angle);
        transform.localScale = new Vector3(scale.x, scale.y * Mathf.Clamp ( 3 * (y1 /y -0.9f),0.1f,10), scale.z);






    }

}

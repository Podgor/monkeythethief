﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour


{
   

    public GameObject movedObj; // объект, который нужно двигать
    public GameObject visualObj; // объект, который мы видим

    public float scaler = 1; // если надо отмасштабировать
    public bool enabler;// включение перспективы
  
   public float minX, maxX;
   public float minY, maxY;

    float coef;
 
    Vector3 objsize;

    void Start()
    {

        minX = transform.position.x - GetComponent<SpriteRenderer>().bounds.extents.x; 
        maxX = transform.position.x + GetComponent<SpriteRenderer>().bounds.extents.x; 
        minY = transform.position.y - GetComponent<SpriteRenderer>().bounds.extents.y; 
        maxY = transform.position.y + GetComponent<SpriteRenderer>().bounds.extents.y;
        objsize = movedObj.transform.localScale;
        
    }
 
    void Update()
    {
        
        float dirX = visualObj.transform.position.x; 
        float dirY = visualObj.transform.position.y;
        if(enabler)
        {
            coef = Mathf.Clamp((visualObj.transform.position.y - minY) / (maxY - minY) + 1, 1, 2) / scaler;
            movedObj.transform.localScale = new Vector3(objsize.x / coef, objsize.y / coef, objsize.z);
        }
        
        if (dirX > maxX ||dirX < minX)
        {
            float x = Mathf.Clamp(dirX, minX, maxX);

            movedObj.transform.position = new Vector3(x - (visualObj.transform.position.x - movedObj.transform.position.x), movedObj.transform.position.y, movedObj.transform.position.z);
        }

        if (dirY > maxY || dirY < minY)
        {
            float y = Mathf.Clamp(dirY, minY, maxY);

            movedObj.transform.position = new Vector3(movedObj.transform.position.x, y - (visualObj.transform.position.y- movedObj.transform.position.y), movedObj.transform.position.z);
        }
    }


}
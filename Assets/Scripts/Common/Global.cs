﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public static class Global 
{
    public static int cur_level = 1;
    public static int pack = 1;
    public static int last_lvl = 9;
    public static string[,,] Hints = new string[15, 10, 10];

    public static string[,,] Graphic_Hints = new string[15, 10, 10];

    public static string[,] Tasks = new string[15, 10];
    public static bool [] yesitwas = new bool[3];
    public static bool muze = false, busy = false;
    public static float screenheight = (Screen.height <= 1920) ? 1920 / Screen.height : 0.9f;
    public static float screenwidth = (Screen.width <= 1080) ? 1080 / Screen.width : 0.9f;
    public static bool stop_anim = false;

    public static void complete_level()
    {
        if (cur_level == last_lvl)
        {
            pack = 2;
            PlayerPrefs.SetInt("pack", 2);
            cur_level = 1;
            PlayerPrefs.SetInt("cur_level", 1);
        }
        else
        {
            cur_level++;
            PlayerPrefs.SetInt("cur_level", cur_level);
        }
        load_level();
    }

    public static void load_level()
    {
        GameObject.Find("todolist").GetComponent<todo_list>().cross_out();
        
        //SceneManager.LoadScene("level_" + pack + "-" + cur_level);
    }

    public static void restart()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        load_level();
        
    }

    public static void import_hints()
    {
        TextAsset hint_table = Resources.Load<TextAsset>("hint_table");

        string[] data = hint_table.text.Split(new char[] { '\n' });

        for (int i = 1; i < data.Length - 1; i++)
        {
            string[] row = data[i].Split(new char[] { ';' });

            int pack_ = int.Parse(row[0]);
            int level_ = int.Parse(row[1]);
            
            for (int j = 1; j<=10; j++)
            {
                Hints[pack_-1, level_-1, j-1]  = row[j + 2];
            }

            Tasks[pack_-1, level_-1] = row[2];
        }

    }

    


    public static void get_help(int _pack, int _level, int i, bool show, GameObject hint_text, GameObject background, GameObject go)
    {
        if (Hints[_pack - 1, _level - 1, i] != "")
        {
            if (!show)
            {
                //Debug.Log("!show");
                //hint_text.SetActive(true);
                //background.SetActive(true);

                hint_text.GetComponent<Text>().text = Hints[_pack - 1, _level - 1, i];
                go.GetComponent<help>().show = true;

            }
            else
            {
                //Debug.Log("show");
                //hint_text.SetActive(false);
                //background.SetActive(false);
                //go.GetComponent<Image>().sprite = Resources.Load<Sprite>("hint_show");
                go.GetComponent<help>().show = false;
                if (i!=10 && (Hints[_pack - 1, _level - 1, i + 1] == "" || Hints[_pack - 1, _level - 1, i + 1] == null))
                {
                    go.GetComponent<Image>().color = new Color32(120, 120, 120, 100);
                    go.GetComponent<Button>().interactable = false;
                }
                else
                {
                    go.GetComponent<help>().i++;
                }


            }
        }

    }

    



}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale_corr : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localScale.x < 0.15)
        {
            transform.localScale = new Vector3(0.15f, 0.15f, 1);
        }
    }
}

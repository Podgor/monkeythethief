﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touch : MonoBehaviour
{


    // turn 0 - front 1 - left  2 - right  3 - back

public string turn = "0";

    void Start()
    {
        Invoke("Idle", 10f);
    }

    // Update is called once per frame
    void OnMouseDown()
    {
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if(child.gameObject.GetComponent<Animator>()) child.gameObject.GetComponent<Animator>().SetBool("touch", true);
        }

        }
    void OnMouseUp()
    {
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.gameObject.GetComponent<Animator>()) child.gameObject.GetComponent<Animator>().SetBool("touch", false);
        }
    }

    void Idle()
    {
        int a = Random.Range(0, 3);
        var children = GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.gameObject.GetComponent<Animator>()) child.GetComponent<Animator>().SetInteger("idle_move", a); 
        }



        Invoke("Idle", 10f);
    }

    public void TouchButton(int i)
    {

        GetComponent<Animator>().SetInteger("turn", i);

        //switch (turn)
        //{


        //    case "0":
        //        GetComponent<Animator>().SetInteger("turn", 0);
        //        break;
        //    case "1":
        //        GetComponent<Animator>().SetInteger("turn", 1);
        //        break;
        //    case "2":
        //        GetComponent<Animator>().SetInteger("turn", 2);
        //        break;
        //    case "3":
        //        GetComponent<Animator>().SetInteger("turn", 3);
        //        break;
        //}
    }


}

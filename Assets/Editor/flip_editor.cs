﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public enum angles
{
    standart = 0,
    flip_180 = 1,
    tilt_left_90,
    tilt_right_90

}

[CustomEditor(typeof(flip_condition))]
public class flip_editor : Editor
{
    public angles tilt;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        flip_condition flip = (flip_condition)target;

        tilt = (angles)EditorGUILayout.EnumPopup("flip angle:", tilt);

        if (GUILayout.Button("set!"))
        {
            switch (tilt)
            {
                case angles.standart:
                    flip.settings = new Vector3(0, -1, 0);
                    break;

                case angles.flip_180:
                    flip.settings = new Vector3(0, 1, 0);
                    break;

                case angles.tilt_left_90:
                    flip.settings = new Vector3(-1, 0, 0);
                    break;

                case angles.tilt_right_90:
                    flip.settings = new Vector3(1, 0, 0);
                    break;

                default:
                    Debug.Log("unrecognized action");
                    break;
            }
        }

        
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class animals : EditorWindow
{
    //public abstract class Dog : MonoBehaviour
    //{
    //    public string name;
    //    public Dog(string name)
    //    {
    //        this.name = name;
    //    }
    //}

    //public class Small_Dog : Dog
    //{
    //    public Small_Dog(string name) : base(name)
    //    {
    //    }
    //}

    [SerializeField] List<GameObject> dog_go_list = new List<GameObject>();
    [SerializeField] List<Dog> dog_list = new List<Dog>();

    [MenuItem("Windows/doggy")]
    public static void ShowWindow()
    {
        GetWindow<animals>("doggy");
    }

    private void OnGUI()
    {
        
        dog_list.Clear();
        dog_list.AddRange(FindObjectsOfType<Dog>());
        dog_go_list.Clear();
        foreach (Dog dog in dog_list)
            dog_go_list.Add(dog.gameObject);
        if (GUILayout.Button("add dog"))
        {
            GameObject go = new GameObject("Bob", typeof(Small_Dog));
            dog_go_list.Add(go);
            dog_list.Add(go.GetComponent<Dog>());
        }

        if (GUILayout.Button("Show dog"))
        {
            foreach (Dog dog in dog_list)
                Debug.Log(dog.name);
        }
        for (int i = 0 ; i < dog_list.Count; i++)
            EditorGUILayout.ObjectField(dog_list[i], typeof(Dog),true);
    }

}

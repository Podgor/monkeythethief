﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;


public enum options
{
    pos_and_rotate = 0,
    tap = 1,
    double_tap = 2,
    join_with = 3,
    leave_place = 4,
    tap_up = 5,
    shake_phone = 6,
    flip_phone = 7,
    empty = 8,
    leave_obj = 9

}

public enum aims
{
    pos_and_rotate = 0,
    on_off_switch = 1,
    victory = 2,
    empty = 3,
    defeat = 4,
    sound = 5

}

public class conditioning : EditorWindow
{
    public options op;
    public aims sel_aim;
    public GameObject _condition;
    List<start_pos> starts = new List<start_pos>();

    public class start_pos
    {
        public GameObject obj;
        public Vector2 pos;
        public float rot;

        public start_pos(GameObject obj, Vector2 pos, float rot)
        {
            this.obj = obj;
            this.pos = pos;
            this.rot = rot;

        }

        public void return_to_start()
        {
            obj.transform.position = pos;
            obj.transform.rotation = Quaternion.Euler(0, 0, rot);

        }
    }




    [SerializeField] List<GameObject> cond_go_list = new List<GameObject>();
    [SerializeField] List<condition> cond_list = new List<condition>();

    [SerializeField] List<GameObject> aim_go_list = new List<GameObject>();
    [SerializeField] List<aim> aim_list = new List<aim>();


    [MenuItem("instruments/constructor")]

    public static void ShowWindow()
    {
        GetWindow<conditioning>("constructor");
    }

    bool use_local_cords = false;

    private void OnGUI()
    {
        cond_list.Clear();
        cond_list.AddRange(FindObjectsOfType<condition>());
        cond_go_list.Clear();
        foreach (condition dog in cond_list)
            cond_go_list.Add(dog.gameObject);

        if (GUILayout.Button("press to load hints"))
        {
            Global.import_hints();
        }

        if (GUILayout.Button("save positions"))
        {
            starts.Clear();
            foreach (GameObject Go in GameObject.FindObjectsOfType<GameObject>())
            {
                starts.Add(new start_pos(Go, Go.transform.position, Go.transform.eulerAngles.z));
            }
        }

        if (GUILayout.Button("return positions"))
        {
            foreach (start_pos s in starts)
            {
                s.return_to_start();
            }
        }

        if (GUILayout.Button("синхронизировать"))
        {
            if (GameObject.Find("conditions"))
            {
                GameObject.Find("conditions").transform.position = Vector3.zero;
            }

            foreach (transform_condition gam in FindObjectsOfType<transform_condition>())
            {
                gam.transform.position = gam.pos;
            }

            foreach (leave_condition gam in FindObjectsOfType<leave_condition>())
            {
                gam.transform.position = gam.pos;
            }
            foreach (transform_aim gam in FindObjectsOfType<transform_aim>())
            {
                gam.transform.position = gam.pos;
            }
            foreach (transform_y_condition gam in FindObjectsOfType<transform_y_condition>())
            {
                gam.transform.position = gam.pos;
            }

            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        }

        GUILayout.Label("Scene:", EditorStyles.boldLabel);

        //if (GUILayout.Button("create new scene"))
        //{
        //    EditorSceneManager.SaveScene(SceneManager.GetActiveScene(), "Assets/Scenes/" + SceneManager.GetActiveScene().name + ".unity");

        //    char sep = '_';
        //    string[] temp = SceneManager.GetActiveScene().name.Split(sep);
        //    if (temp.Length == 2)
        //    {
        //        Scene n_s = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
        //        string num = temp[1];
        //        string next_name = "level_" + (int.Parse(temp[1]) + 1).ToString();
        //        GameObject camera = new GameObject();
        //        camera.AddComponent<Camera>();
        //        camera.AddComponent<AudioListener>();
        //        camera.AddComponent<main_condition>();
        //        camera.name = "Main Camera";
        //        EditorSceneManager.SaveScene(n_s, "Assets/Scenes/" + next_name + ".unity");
        //    }

        //}
        if (GUILayout.Button("add canvas"))
        {
            Canvas can = Resources.Load<Canvas>("Canvas_prefab");
            Canvas the_can = Instantiate(can, Vector3.zero, Quaternion.identity);
        }


        if (GUILayout.Button("set or create active object"))
        {
            foreach (GameObject sel in Selection.gameObjects)
            {
                if (sel != null && sel.GetComponent<BoxCollider2D>() == null && sel.GetComponent<active_obj>() == null)
                {
                    sel.AddComponent<active_obj>();
                    sel.AddComponent<BoxCollider2D>();
                }
                //else
                //{
                //    GameObject obj = new GameObject();
                //    obj.name = "new active object";
                //    obj.AddComponent<SpriteRenderer>();
                //    obj.AddComponent<active_obj>();
                //}
            }
        }

        op = (options)EditorGUILayout.EnumPopup("Choose condition:", op);

        use_local_cords = GUILayout.Toggle(use_local_cords, "use local coordinates");


        if (GUILayout.Button("create simple condition"))
        {
            switch (op)
            {
                case options.pos_and_rotate:
                    GameObject GO = Selection.gameObjects[0];
                    //_condition = new transform_condition(GO, GO.transform.position, GO.transform.eulerAngles.z);
                    GameObject go = new GameObject("the transform condition", typeof(transform_condition));
                    cond_go_list.Add(go);
                    go.GetComponent<transform_condition>().obj = GO;

                    if (!use_local_cords)
                    {
                        go.GetComponent<transform_condition>().pos = GO.transform.position;
                        go.GetComponent<transform_condition>().rot = GO.transform.eulerAngles.z;
                        Debug.Log(GO.transform.eulerAngles.z);
                    }
                    else
                    {
                        go.GetComponent<transform_condition>().pos = GO.transform.localPosition;
                        go.GetComponent<transform_condition>().rot = GO.transform.localEulerAngles.z;
                    }

                    go.GetComponent<transform_condition>().use_local = use_local_cords;
                    go.GetComponent<transform_condition>().rot = GO.transform.eulerAngles.z;
                    go.GetComponent<transform_condition>().once = true;
                    go.GetComponent<transform_condition>().acted = false;
                    go.GetComponent<transform_condition>().approx = 0.4f;
                    go.name = "precondition";

                    Debug.Log(op + " condition created");
                    break;

                case options.tap:
                    GameObject GO2 = Selection.gameObjects[0];
                    
                    GameObject go2 = new GameObject("the transform condition", typeof(tap_condition));
                    cond_go_list.Add(go2);
                    go2.GetComponent<tap_condition>().obj = GO2;
                    go2.GetComponent<tap_condition>().once = true;
                    go2.GetComponent<tap_condition>().acted = false;
                    go2.name = "precondition";

                    Debug.Log(op + " condition created");
                    break;

                case options.join_with:
                    GameObject go3 = new GameObject("the fuck", typeof(join_condition));
                    go3.GetComponent<join_condition>().G1 = Selection.gameObjects[0];
                    go3.GetComponent<join_condition>().G2 = Selection.gameObjects[1];
                    go3.GetComponent<join_condition>().once = true;
                    go3.GetComponent<join_condition>().acted = false;
                    go3.name = "precondition";
                    Debug.Log(op + " condition created");

                    break;
                case options.leave_place:
                    GameObject GO4 = Selection.gameObjects[0];
                    //_condition = new transform_condition(GO, GO.transform.position, GO.transform.eulerAngles.z);
                    GameObject go4 = new GameObject("the transform condition", typeof(leave_condition));
                    cond_go_list.Add(go4);
                    go4.GetComponent<leave_condition>().obj = GO4;

                    if (!use_local_cords)
                    {
                        go4.GetComponent<leave_condition>().pos = GO4.transform.position;
                    }
                    else
                    {
                        go4.GetComponent<leave_condition>().pos = GO4.transform.localPosition;
                    }

                    go4.GetComponent<leave_condition>().use_local = use_local_cords;
                    go4.GetComponent<leave_condition>().rot = GO4.transform.eulerAngles.z;
                    go4.GetComponent<leave_condition>().once = true;
                    go4.GetComponent<leave_condition>().acted = false;
                    go4.GetComponent<leave_condition>().approx = 0.3f;
                    go4.name = "precondition";

                    Debug.Log(op + " condition created");
                    break;

                case options.tap_up:

                    GameObject GO5 = Selection.gameObjects[0];

                    GameObject go5 = new GameObject("condition", typeof(tap_up_condition));
                    cond_go_list.Add(go5);
                    go5.GetComponent<tap_up_condition>().obj = GO5;
                    go5.GetComponent<tap_up_condition>().once = true;
                    go5.GetComponent<tap_up_condition>().acted = false;
                    go5.GetComponent<tap_up_condition>().firstly_tapped = false;
                    go5.name = "precondition";

                    Debug.Log(op + " condition created");
                    break;
                case options.shake_phone:
                    GameObject go6 = new GameObject("the transform condition", typeof(shake_condition));
                    cond_go_list.Add(go6);
                    go6.GetComponent<shake_condition>().once = true;
                    go6.GetComponent<shake_condition>().acted = false;
                    go6.name = "precondition";

                    Debug.Log(op + " condition created");
                    break;

                case options.flip_phone:
                    GameObject go7 = new GameObject("the transform condition", typeof(flip_condition));
                    cond_go_list.Add(go7);
                    go7.GetComponent<flip_condition>().once = true;
                    go7.GetComponent<flip_condition>().acted = false;
                    go7.name = "precondition";

                    Debug.Log(op + " condition created");
                    break;

                case options.empty:
                    GameObject go8 = new GameObject("precondition", typeof(empty_condition));
                    cond_go_list.Add(go8);
                    go8.GetComponent<empty_condition>().once = true;
                    go8.GetComponent<empty_condition>().acted = false;
                    break;

                case options.double_tap:
                    GameObject go9 = new GameObject("precondition", typeof(double_tap_cond));
                    cond_go_list.Add(go9);
                    go9.GetComponent<double_tap_cond>().once = true;
                    go9.GetComponent<double_tap_cond>().acted = false;
                    go9.GetComponent<double_tap_cond>().obj = Selection.gameObjects[0];
                    break;

                case options.leave_obj:

                    GameObject go10 = new GameObject("precondition", typeof(leaveobj_condition));
                    cond_go_list.Add(go10);
                    go10.GetComponent<leaveobj_condition>().once = true;
                    go10.GetComponent<leaveobj_condition>().acted = false;
                    go10.GetComponent<leaveobj_condition>().G1 = Selection.gameObjects[0];
                    go10.GetComponent<leaveobj_condition>().approx = 0.4f;
                    break;


                default:
                    Debug.Log("error: unrecognized action");

                    break;

            }
        }

        sel_aim = (aims)EditorGUILayout.EnumPopup("Choose aim:", sel_aim);

        if (GUILayout.Button("add aim"))
        {
            

            switch (sel_aim)
            {

                case aims.pos_and_rotate:
                    GameObject GO = Selection.gameObjects[0];
                    GameObject a = new GameObject("the transform aim", typeof(transform_aim));
                    a.GetComponent<transform_aim>().G = GO;
                    if (!use_local_cords)
                    {
                        a.GetComponent<transform_aim>().pos = GO.transform.position;
                    }
                    else
                    {
                        a.GetComponent<transform_aim>().pos = GO.transform.localPosition;
                    }
                    
                    a.GetComponent<transform_aim>().rot = GO.transform.eulerAngles.z;
                    if (Selection.gameObjects[0].GetComponent<condition>() == null)
                    {
                        a.transform.SetParent(GameObject.Find("precondition").transform);
                    }
                    else
                    {
                        a.transform.SetParent(Selection.gameObjects[0].transform);
                    }
                    //GameObject.Find("precondition").GetComponent<condition>().aims.Add(a.GetComponent<transform_aim>());

                    Debug.Log(sel_aim + " aim added");
                    break;

                case aims.on_off_switch:
                    GameObject GO2 = Selection.gameObjects[0];
                    GameObject a2 = new GameObject("the switch aim", typeof(switch_aim));
                    a2.GetComponent<switch_aim>().G = GO2;
                    if (Selection.gameObjects[0].GetComponent<condition>() == null)
                    {
                        a2.transform.SetParent(GameObject.Find("precondition").transform);
                    }
                    else
                    {
                        a2.transform.SetParent(Selection.gameObjects[0].transform);
                    }
                    break;

                case aims.empty:
                    GameObject a3 = new GameObject("the empty aim", typeof(empty_aim));

                    if (Selection.gameObjects[0].GetComponent<condition>() == null)
                    {
                        a3.transform.SetParent(GameObject.Find("precondition").transform);
                    }
                    else
                    {
                        a3.transform.SetParent(Selection.gameObjects[0].transform);
                    }
                    break;

                case aims.victory:
                    GameObject a4 = new GameObject("the victory", typeof(victory_aim));
                    if (Selection.gameObjects[0].GetComponent<condition>() == null)
                    {
                        a4.transform.SetParent(GameObject.Find("precondition").transform);
                    }
                    else
                    {
                        a4.transform.SetParent(Selection.gameObjects[0].transform);
                    }
                    break;

                case aims.defeat:
                    GameObject a5 = new GameObject("the defeat", typeof(loose_aim));
                    if (Selection.gameObjects[0].GetComponent<condition>() == null)
                    {
                        a5.transform.SetParent(GameObject.Find("precondition").transform);
                    }
                    else
                    {
                        a5.transform.SetParent(Selection.gameObjects[0].transform);
                    }
                    break;

                case aims.sound:
                    GameObject a6 = new GameObject("the sound aim", typeof(sound_aim));

                    if (Selection.gameObjects[0].GetComponent<condition>() == null)
                    {
                        a6.transform.SetParent(GameObject.Find("precondition").transform);
                    }
                    else
                    {
                        a6.transform.SetParent(Selection.gameObjects[0].transform);
                    }
                    break;

                default:
                    Debug.Log("error: problem aim");
                    break;
            }
        }

        if (GUILayout.Button("Add condition"))// && _condition != null)
        {

            cond_go_list.Add(GameObject.Find("precondition"));

            cond_list.Add(GameObject.Find("precondition").GetComponent<condition>());

            GameObject.Find("precondition").name = "the condition" + (cond_list.Count-1).ToString();


            //cond_list.Add(_condition);
            //cond_go_list.Add(_condition.gameObject);
            //cond_list.Add(_condition);
            //_condition = null;
            

        }

        //if (GUILayout.Button("chain two conditions"))
        //{
        //    Selection.gameObjects[1].GetComponent<condition>().chain = Selection.gameObjects[0].GetComponent<condition>();
        //}

        if (GUILayout.Button("cancel condition"))
        {
            Destroy(GameObject.Find("precondition"));
        }

        //if (GUILayout.Button("clear all conditions"))
        //{
        //    foreach (condition gobj in GameObject.FindObjectsOfType<condition>())
        //    {
        //         DestroyImmediate(gobj.gameObject);
        //    }
        //}

        
        
        
        //if (GUILayout.Button("add dog"))
        //{
        //    GameObject go = new GameObject("Bob", typeof(Small_Dog));
        //    cond_go_list.Add(go);
        //    cond_list.Add(go.GetComponent<condition>());
        //}

        //if (GUILayout.Button("Show dog"))
        //{
        //    foreach (condition dog in cond_list)
        //        Debug.Log(dog.GetType());
        //}
        for (int i = 0; i < cond_list.Count; i++)
            EditorGUILayout.ObjectField(cond_list[i], typeof(condition), true);
    }

    // Update is called once per frame

    void Update()
    {
        //Debug.Log(cond_list.Count);
        if (EditorApplication.isPlaying)
       {
            //foreach (condition the_dog in cond_list)
            //{
            //    //if (the_dog.aims.Count <= 0)
            //    //{
            //    //    the_dog.aims.AddRange(the_dog.GetComponentsInChildren<aim>());
            //    //}

            //    if (the_dog.check() && the_dog.acted==false)
            //    {
            //        if (the_dog.chain == null && the_dog.block_chain == null)
            //        {
            //            the_dog.act();
            //            if (the_dog.once)
            //            {
            //                the_dog.acted = true;
            //            }
            //        }
            //        else if (the_dog.chain != null && the_dog.block_chain==null)
            //        {
            //            if (the_dog.chain.acted)
            //            {
            //                the_dog.act();
            //                if (the_dog.once)
            //                {
            //                    the_dog.acted = true;
            //                }
            //            }
            //        }
            //        else if (the_dog.chain == null && the_dog.block_chain != null)
            //        {
            //            if (the_dog.block_chain.acted==false)
            //            {
            //                the_dog.act();
            //                if (the_dog.once)
            //                {
            //                    the_dog.acted = true;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (the_dog.chain.acted == true && the_dog.block_chain.acted == false)
            //            {
            //                the_dog.act();
            //                if (the_dog.once)
            //                {
            //                    the_dog.acted = true;
            //                }
            //            }
            //        }
                    
            //    }
            //}
       }
    }
}

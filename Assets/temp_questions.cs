﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class temp_questions : MonoBehaviour
{
    public List<GameObject> clones = new List<GameObject>();
    bool qq;
    

    void Start()
    {
        GameObject quest = GameObject.Find("quest");
        
        for(int i=0; i< 200; i++)
		{
            Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height),10));
            GameObject clone = Instantiate(quest, pos, Quaternion.identity);
            clones.Add(clone);
            float r = Random.Range(0f, 1f);
            
            clones[i].transform.localScale = new Vector3(r/3.5f, r/3.5f, 1);
            clones[i].GetComponent<SpriteRenderer>().color = new Color32(255,255,255,(byte)(150*r));
            Move(i, 6);
            
        }
           
    }

    // Update is called once per frame
    void Move(int i, int h)
    {
        iTween.MoveTo(clones[i], iTween.Hash("y", h, "time", Random.Range(4f, 15f)));
    }

    void Update()
	{
        for (int j = 0; j < 200; j++)
        {
            int hmax = (int)clones[j].transform.position.y;
            if(hmax>6 && !qq)
			{
                clones[j].transform.position = new Vector3(clones[j].transform.position.x, 0, 1);
                Move(j, 6);
            }

        }
    }

    void OnMouseDown()
	{
        qq = true;
	}

}
